/**
 * Created by Даниил on 31.01.2017.
 */

$(document).ready(function () {
    $('#confirm').click(function () {
        if (!validatePhones()) {
            $('#dialogBody').text('Enter correct number');
            $('#addPhoneModal').modal('show');
        } else if ($("input[name=user]").val() == "") {
            $('#dialogBody').text("User name can't be empty!");
            $('#addPhoneModal').modal('show');
        } else if ($("input[name=password]").val() == "") {
            $('#dialogBody').text("Password can't be empty!");
            $('#addPhoneModal').modal('show');
        } else if ($("input[name=email]").val() == "") {
            $('#dialogBody').text("Email can't be empty!");
            $('#addPhoneModal').modal('show');
        } else {
            $('#confirmModal').modal('show');
        }
    });
});