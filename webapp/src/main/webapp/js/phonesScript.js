/**
 * Created by Даниил on 30.01.2017.
 */

$(document).ready(function () {
    function rearrange() {
        var i = 0;
        $('#pTable tr').each(function () {
            $(this).find(".pNo").html(i);
            i++;
        });
    }

    $(document).on('click', '#rButton', function () {
        $(this).closest('tr').remove();
        rearrange();
    });

    $('#addButId').click(function () {
        if (validatePhones()) {
            var sno = $('#pTable tr').length;
            var text = "<tr><td class='pNo'>" + sno + "</td>" +
                "<td><input name='homeNumbers' type='text'></td>" +
                "<td><select name='phoneType'><option value='home' selected='selected'>Home</option><option value='work'>Work</option></select></td>" +
                "<td><button type='button' id='rButton' class='btn btn-primary'>Remove</button></td></tr>";
            $('#pTable').append(text);
            $('select[name=phoneType]').change(function () {
                var phoneName = $(this).find('option:selected').val();
                return $(this).closest('td').prev().find('input[type=text]').attr('name', phoneName + 'Numbers');
            })
        } else {
            $('#dialogBody').text('Enter correct number');
            $('#addPhoneModal').modal('show');
        }
    });
});

var validatePhones = function () {
    var valid = true;
    $('input[name=homeNumbers]').each(function () {
        var phoneValue = $(this).val();
        if (phoneValue == '' || !phoneValue.match(/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/i)) {
            return valid = false;
        }
    });
    $('input[name=workNumbers]').each(function () {
        var phoneValue = $(this).val();
        if (phoneValue == '' || !phoneValue.match(/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/i)) {
            return valid = false;
        }
    });
    return valid;
};