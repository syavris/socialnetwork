/**
 * Created by Даниил on 05.03.2017.
 */

$(document).ready(function () {
    var recordsPerPage = 5;
    var accountRecordsToGet = recordsPerPage;
    var groupRecordsToGet = recordsPerPage;
    var totalAccountPages = Math.floor(jspVariables.totalAccountRecords / recordsPerPage);
    var totalGroupPages = Math.floor(jspVariables.totalGroupRecords / recordsPerPage);
    var currentAccountPage = 1;
    var currentGroupPage = 1;
    var currentAccountIndex = 0;
    var currentGroupIndex = 0;
    if (jspVariables.totalAccountRecords % recordsPerPage != 0) {
        totalAccountPages++;
    }
    if (jspVariables.totalAccountRecords < recordsPerPage) {
        accountRecordsToGet = jspVariables.totalAccountRecords % recordsPerPage;
    } else {
        accountRecordsToGet = recordsPerPage;
    }
    if (jspVariables.totalGroupRecords % recordsPerPage != 0) {
        totalGroupPages++;
    }
    if (jspVariables.totalGroupRecords < recordsPerPage) {
        groupRecordsToGet = jspVariables.totalGroupRecords % recordsPerPage;
    } else {
        groupRecordsToGet = recordsPerPage;
    }
    $("#accountPage").html("Page " + currentAccountPage + " of " + totalAccountPages);
    $("#groupPage").html("Page " + currentGroupPage + " of " + totalGroupPages);
    $.get(jspVariables.contextPath + "/getAccounts?startIndex=" + currentAccountIndex + "&recordsToGet=" + accountRecordsToGet + "&filter=" + jspVariables.filter, function (data) {
        for (i = 0; i < accountRecordsToGet; i++) {
            $("#accountsResult").append('<img src=' + jspVariables.contextPath + '/image?photoName=' + data[i].photo + ' class="img-circle" width="50" height="50">');
            $("#accountsResult").append("<a href=" + jspVariables.contextPath + "/account?id=" + data[i].id + ">" + (currentAccountIndex + 1) + ". " + data[i].name + "</a>" + "<br/>");
            currentAccountIndex++;
        }
        if (currentAccountPage == totalAccountPages) {
            $("#accountNext").hide();
        } else {
            $("#accountNext").show();
        }
        if (currentAccountPage == 1) {
            $("#accountBack").hide();
        } else {
            $("#accountBack").show();
        }
    });
    $.get(jspVariables.contextPath + "/getGroups?startIndex=" + currentGroupIndex + "&recordsToGet=" + groupRecordsToGet + "&filter=" + jspVariables.filter, function (data) {
        for (i = 0; i < groupRecordsToGet; i++) {
            $("#groupsResult").append('<img src=' + jspVariables.contextPath + '/image?photoName=' + data[i].photo + ' class="img-circle" width="50" height="50">');
            $("#groupsResult").append("<a href=" + jspVariables.contextPath + "/group?id=" + data[i].id + ">" + (currentGroupIndex + 1) + ". " + data[i].name + "</a>" + "<br/>");
            currentGroupIndex++;
        }
        if (currentGroupPage == totalGroupPages) {
            $("#groupNext").hide();
        } else {
            $("#groupNext").show();
        }
        if (currentGroupPage == 1) {
            $("#groupBack").hide();
        } else {
            $("#groupBack").show();
        }
    });
    $("#accountNext").click(function () {
        $.get(jspVariables.contextPath + "/getTotalAccounts?filter=" + jspVariables.filter, function (data) {
            jspVariables.totalAccountRecords = data;
            totalAccountPages = Math.ceil(jspVariables.totalAccountRecords / recordsPerPage);
            $("#accountsResult").html("");
            currentAccountPage++;
            if (currentAccountPage == totalAccountPages) {
                $("#accountNext").hide();
                if (jspVariables.totalAccountRecords % recordsPerPage != 0) {
                    accountRecordsToGet = jspVariables.totalAccountRecords % recordsPerPage;
                } else {
                    accountRecordsToGet = recordsPerPage;
                }
            } else {
                $("#accountNext").show();
                accountRecordsToGet = recordsPerPage;
            }
            if (currentAccountPage == 1) {
                $("#accountBack").hide();
            } else {
                $("#accountBack").show();
            }
            $.get(jspVariables.contextPath + "/getAccounts?startIndex=" + currentAccountIndex + "&recordsToGet=" + accountRecordsToGet + "&filter=" + jspVariables.filter, function (data) {
                for (i = 0; i < accountRecordsToGet; i++) {
                    $("#accountsResult").append('<img src=' + jspVariables.contextPath + '/image?photoName=' + data[i].photo + ' class="img-circle" width="50" height="50">');
                    $("#accountsResult").append("<a href=" + jspVariables.contextPath + "/account?id=" + data[i].id + ">" + (currentAccountIndex + 1) + ". " + data[i].name + "</a>" + "<br/>");
                    currentAccountIndex++;
                }
            });
            $("#accountPage").html("Page " + currentAccountPage + " of " + totalAccountPages);
        });
    });
    $("#groupNext").click(function () {
        $("#groupsResult").html("");
        $.get(jspVariables.contextPath + "/getTotalGroups?filter=" + jspVariables.filter, function (data) {
            jspVariables.totalGroupRecords = data;
            totalGroupPages = Math.ceil(jspVariables.totalGroupRecords / recordsPerPage);
            currentGroupPage++;
            if (currentGroupPage == totalGroupPages) {
                $("#groupNext").hide();
                if (jspVariables.totalGroupRecords % recordsPerPage != 0) {
                    groupRecordsToGet = jspVariables.totalGroupRecords % recordsPerPage;
                } else {
                    groupRecordsToGet = recordsPerPage;
                }
            } else {
                $("#groupNext").show();
                groupRecordsToGet = recordsPerPage;
            }
            if (currentGroupPage == 1) {
                $("#groupBack").hide();
            } else {
                $("#groupBack").show();
            }
            $.get(jspVariables.contextPath + "/getGroups?startIndex=" + currentGroupIndex + "&recordsToGet=" + groupRecordsToGet + "&filter=" + jspVariables.filter, function (data) {
                for (i = 0; i < groupRecordsToGet; i++) {
                    $("#groupsResult").append('<img src=' + jspVariables.contextPath + '/image?photoName=' + data[i].photo + ' class="img-circle" width="50" height="50">');
                    $("#groupsResult").append("<a href=" + jspVariables.contextPath + "/group?id=" + data[i].id + ">" + (currentGroupIndex + 1) + ". " + data[i].name + "</a>" + "<br/>");
                    currentGroupIndex++;
                }
            });
            $("#groupPage").html("Page " + currentGroupPage + " of " + totalGroupPages);
        });
    });
    $("#accountBack").click(function () {
        $("#accountsResult").html("");
        $.get(jspVariables.contextPath + "/getTotalAccounts?filter=" + jspVariables.filter, function (data) {
            jspVariables.totalAccountRecords = data;
            totalAccountPages = Math.ceil(jspVariables.totalAccountRecords / recordsPerPage);
            currentAccountPage--;
            currentAccountIndex = currentAccountIndex - accountRecordsToGet - recordsPerPage;
            if (currentAccountPage == totalAccountPages) {
                $("#accountNext").hide();
                accountRecordsToGet = jspVariables.totalAccountRecords % recordsPerPage;
            } else {
                $("#accountNext").show();
                accountRecordsToGet = recordsPerPage;
            }
            if (currentAccountPage == 1) {
                $("#accountBack").hide();
            } else {
                $("#accountBack").show();
            }
            $.get(jspVariables.contextPath + "/getAccounts?startIndex=" + currentAccountIndex + "&recordsToGet=" + accountRecordsToGet + "&filter=" + jspVariables.filter, function (data) {
                for (i = 0; i < accountRecordsToGet; i++) {
                    $("#accountsResult").append('<img src=' + jspVariables.contextPath + '/image?photoName=' + data[i].photo + ' class="img-circle" width="50" height="50">');
                    $("#accountsResult").append("<a href=" + jspVariables.contextPath + "/account?id=" + data[i].id + ">" + (currentAccountIndex + 1) + ". " + data[i].name + "</a>" + "<br/>");
                    currentAccountIndex++;
                }
            });
            $("#accountPage").html("Page " + currentAccountPage + " of " + totalAccountPages);
        });
    });
    $("#groupBack").click(function () {
        $("#groupsResult").html("");
        $.get(jspVariables.contextPath + "/getTotalGroups?filter=" + jspVariables.filter, function (data) {
            jspVariables.totalGroupRecords = data;
            totalGroupPages = Math.ceil(jspVariables.totalGroupRecords / recordsPerPage);
            currentGroupPage--;
            currentGroupIndex = currentGroupIndex - groupRecordsToGet - recordsPerPage;
            if (currentGroupPage == totalGroupPages) {
                $("#groupNext").hide();
                groupRecordsToGet = jspVariables.totalGroupRecords % recordsPerPage;
            } else {
                $("#groupNext").show();
                groupRecordsToGet = recordsPerPage;
            }
            if (currentGroupPage == 1) {
                $("#groupBack").hide();
            } else {
                $("#groupBack").show();
            }
            $.get(jspVariables.contextPath + "/getGroups?startIndex=" + currentGroupIndex + "&recordsToGet=" + groupRecordsToGet + "&filter=" + jspVariables.filter, function (data) {
                for (i = 0; i < groupRecordsToGet; i++) {
                    $("#groupsResult").append('<img src=' + jspVariables.contextPath + '/image?photoName=' + data[i].photo + ' class="img-circle" width="50" height="50">');
                    $("#groupsResult").append("<a href=" + jspVariables.contextPath + "/group?id=" + data[i].id + ">" + (currentGroupIndex + 1) + ". " + data[i].name + "</a>" + "<br/>");
                    currentGroupIndex++;
                }
            });
            $("#groupPage").html("Page " + currentGroupPage + " of " + totalGroupPages);
        });
    });
});