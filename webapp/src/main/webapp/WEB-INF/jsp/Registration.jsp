<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Даниил
  Date: 25.12.2016
  Time: 20:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/phonesScript.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/confirmDialog.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <link href="${pageContext.request.contextPath}/css/mainPage.css" rel="stylesheet">
    <script>
        $(function () {
            $("#datepicker").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
    <title>Registration</title>
</head>
<body>
<div class="container">
    <form class="form-signin" action="${pageContext.request.contextPath}/registration" method="post"
          enctype="multipart/form-data">

        <h2 class="form-signin-heading">Account information</h2>
        <div class="form-signin-heading" style="color:red">${errorMessage}</div>

        <div class="control-group">
            <label class="control-label" for="name">Your Name</label>
            <div class="controls">
                <input size="50" name="name" id="name" value="" type="text" class="form-control"
                       placeholder="Enter your Name">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="password">Password</label>
            <div class="controls">
                <input size="50" name="password" id="password" value="" type="password" class="form-control"
                       placeholder="Enter your Password">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="email">Your Email</label>
            <div class="controls">
                <input size="50" name="email" id="email" value="" type="text" class="form-control"
                       placeholder="Enter your Email">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="datepicker">Your Birthday</label>
            <div class="controls">
                <input size="50" name="dateOfBirth" id="datepicker" value="" type="text" class="form-control"
                       placeholder="Choose your Birthday" readonly='true'>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Sex</label>
            <div class="cols-sm-10">
                <label class="radio-inline" for="sex-0">
                    <input type="radio" name="sex" id="sex-0" value="MALE" checked="checked">
                    Male
                </label>
                <label class="radio-inline" for="sex-1">
                    <input type="radio" name="sex" id="sex-1" value="FEMALE">
                    Female
                </label>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="home">Your Home Address</label>
            <div class="controls">
                <input size="50" name="homeAddress" id="home" value="" type="text" class="form-control"
                       placeholder="Enter your Home Address">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="work">Your Work Address</label>
            <div class="controls">
                <input size="50" name="workAddress" id="work" value="" type="text" class="form-control"
                       placeholder="Enter your Work Address">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Your Phones</label>
            <table class="table" id="pTable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Number</th>
                    <th>Type</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <button id="addButId" type="button" class="btn btn-large btn-primary btn-block">Add Phone
            </button>

            <div class="modal fade" id="addPhoneModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Error</h4>
                        </div>
                        <div class="modal-body" id="dialogBody">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="icq">Your ICQ</label>
            <div class="controls">
                <input size="50" name="ICQ" id="icq" value="" type="number" class="form-control"
                       placeholder="Enter your ICQ">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="skype">Your Skype</label>
            <div class="controls">
                <input size="50" name="skype" id="skype" value="" type="text" class="form-control"
                       placeholder="Enter your Skype">
            </div>
        </div>

        <label class="btn btn-large btn-primary btn-block">
            Choose your photo <input type="file" name="uploadedPhoto" style="display: none;">
        </label>

        <a href="${pageContext.request.contextPath}/" name="back" id="submit" type="submit"
           class="btn btn-large btn-primary btn-block">Back
        </a>
        <button name="register" id="confirm" value="" type="button" class="btn btn-large btn-primary btn-block">
            Register
        </button>
        <div class="modal fade" id="confirmModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure?
                    </div>
                    <div class="modal-footer">
                        <button name="create" value="" type="submit" class="btn btn-primary">Yes
                        </button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
</body>
</html>
