<%--
  Created by IntelliJ IDEA.
  User: Даниил
  Date: 21.12.2016
  Time: 21:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <link href="${pageContext.request.contextPath}/css/mainPage.css" rel="stylesheet">
    <title>Home</title>
</head>
<body>
<div class="container">
    <form class="form-signin" action="${pageContext.request.contextPath}/" method="post" id="form-signin">
        <h2 class="form-signin-heading">Please sign in</h2>
        <div class="form-signin-heading" style="color:red">${errorMessage}</div>
        <div class="control-group">
            <label class="control-label" for="email">Email:</label>
            <div class="controls">
                <input size="50" name="email" id="email" value="" type="text" class="form-control" placeholder="Email">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="password">Password:</label>
            <div class="controls">
                <input size="50" name="password" id="password" value="" type="password" class="form-control"
                       placeholder="Password">
            </div>
        </div>
        <label class="checkbox">
            <input type="checkbox" name="rememberMe" value="Y"> Remember me
        </label>
        <button name="enter" id="submit" value="" type="submit" class="btn btn-large btn-primary btn-block">Sign in
        </button>
        <a href="${pageContext.request.contextPath}/registration" name="register" id="register" value="" type="submit"
           class="btn btn-large btn-primary btn-block">
            Register
        </a>
    </form>
</div>
</body>
</html>
