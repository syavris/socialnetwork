<%--
  Created by IntelliJ IDEA.
  User: Даниил
  Date: 03.01.2017
  Time: 13:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="https://code.jquery.com/jquery-2.1.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<link href="${pageContext.request.contextPath}/css/mainPage.css" rel="stylesheet">
<html>
<head>
    <title>Create Group</title>
</head>
<body>
<jsp:include page="Menu.jsp"/>

<div class="container">
    <form class="form-signin" action="${pageContext.request.contextPath}/createGroup" method="post"
          enctype="multipart/form-data">

        <h2 class="form-signin-heading">Group information</h2>
        <h2 class="form-signin-heading" style="color:red">${errorMessage}</h2>

        <div class="control-group">
            <label class="control-label" for="name">Group Name</label>
            <div class="controls">
                <input size="50" name="name" id="name" value="" type="text" class="form-control"
                       placeholder="Enter group Name">
            </div>
        </div>

        <label class="btn btn-large btn-primary btn-block">
            Choose group photo <input type="file" name="uploadedPhoto" style="display: none;">
        </label>

        <button name="create" id="confirm" value="" type="submit" class="btn btn-large btn-primary btn-block">
            Create
        </button>
    </form>
</div>
</body>
</html>
