<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Даниил
  Date: 28.12.2016
  Time: 21:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-2.1.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Friends</title>
</head>
<body>
<jsp:include page="Menu.jsp"/>
<div class="container">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#active">Active friends</a></li>
        <li><a data-toggle="tab" href="#inactive">Inactive friends</a></li>
    </ul>
    <div class="tab-content">
        <div id="active" class="tab-pane fade in active">
            <c:forEach items="${Active}" var="account">
                <img src=${pageContext.request.contextPath}/image?photoName=${account.photo} class="img-circle"
                     width="50" height="50">
                <a href="${pageContext.request.contextPath}/account?id=${account.id}">${account.name}</a>
                <form action="${pageContext.request.contextPath}/friends" method="post">
                    <button type="submit" name="delete" value="${account.id}" class="btn btn-primary">Delete</button>
                </form>
            </c:forEach>
        </div>
        <div id="inactive" class="tab-pane fade">
            <c:forEach items="${Inactive}" var="account">
                <img src=${pageContext.request.contextPath}/image?photoName=${account.photo} class="img-circle"
                     width="50" height="50">
                <a href="${pageContext.request.contextPath}/account?id=${account.id}">${account.name}</a>
                <form action="${pageContext.request.contextPath}/friends" method="post">
                    <button type="submit" name="delete" value="${account.id}" class="btn btn-primary">Delete</button>
                    <button type="submit" name="add" value="${account.id}" class="btn btn-primary">Add to Friend
                    </button>
                </form>
            </c:forEach>
        </div>
    </div>
</div>
</body>
</html>
