<%--
  Created by IntelliJ IDEA.
  User: Даниил
  Date: 06.01.2017
  Time: 13:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-2.1.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>SearchResult</title>
</head>
<body>
<jsp:include page="Menu.jsp"/>
<div class="container">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#accounts">Accounts</a></li>
        <li><a data-toggle="tab" href="#groups">Groups</a></li>
    </ul>
    <div class="tab-content">
        <div id="accounts" class="tab-pane fade in active">
            <div id="accountsResult"></div>
            <br/>
            <button id="accountBack" class="btn btn-primary">Back</button>
            <button id="accountNext" class="btn btn-primary">Next</button>
            <p id="accountPage"></p>
        </div>
        <div id="groups" class="tab-pane fade">
            <div id="groupsResult"></div>
            <br/>
            <button id="groupBack" class="btn btn-primary">Back</button>
            <button id="groupNext" class="btn btn-primary">Next</button>
            <p id="groupPage"></p>
        </div>
    </div>
</div>

<script type="text/javascript">
    var jspVariables = {
        totalAccountRecords: ${accountSize},
        totalGroupRecords: ${groupSize},
        contextPath: "${pageContext.request.contextPath}",
        filter: "${filter}"
    }
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/myPagination.js"></script>
</body>
</html>
