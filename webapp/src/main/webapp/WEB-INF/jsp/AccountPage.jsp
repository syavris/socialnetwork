<%--
  Created by IntelliJ IDEA.
  User: Даниил
  Date: 26.12.2016
  Time: 21:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <script src="https://code.jquery.com/jquery-2.1.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <link href="${pageContext.request.contextPath}/css/accountPage.css" rel="stylesheet">
    <title>Account page</title>
</head>
<body>
<jsp:include page="Menu.jsp"/>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad">


            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">${AccountPage.name}</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 " align="center"><img
                                src="${pageContext.request.contextPath}/image?photoName=${AccountPage.photo}"
                                class="img-circle img-responsive"></div>

                        <div class=" col-md-9 col-lg-9 ">
                            <table class="table table-user-information">
                                <tbody>
                                <tr>
                                    <td>Sex:</td>
                                    <td>${AccountPage.sex}</td>
                                </tr>
                                <tr>
                                    <td>Birthday:</td>
                                    <td><fmt:formatDate value="${AccountPage.dateOfBirth}" pattern="yyyy-MM-dd"/></td>
                                </tr>
                                <tr>
                                    <td>Home address:</td>
                                    <td>${AccountPage.homeAddress}</td>
                                </tr>

                                <tr>
                                <tr>
                                    <td>Work address:</td>
                                    <td>${AccountPage.workAddress}</td>
                                </tr>
                                <tr>
                                    <td>ICQ:</td>
                                    <td><c:if test="${AccountPage.ICQ != 0}"><b>${AccountPage.ICQ}</b></c:if></td>
                                </tr>
                                <tr>
                                    <td>Skype:</td>
                                    <td>${AccountPage.skype}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>${AccountPage.email}</td>
                                </tr>
                                <td>Phone Number</td>
                                <td>
                                    <c:forEach items="${AccountPage.homeNumbers}" var="home">
                                        ${home}(Home)<br>
                                    </c:forEach>
                                    <c:forEach items="${AccountPage.workNumbers}" var="work">
                                        ${work}(Work)<br>
                                    </c:forEach>
                                </td>

                                </tbody>
                            </table>

                            <c:if test="${AccountPage.id != ActiveAccount.id && isFriend == false}">
                                <form action="${pageContext.request.contextPath}/account" method="post">
                                    <button type="submit" name="addToFriend" value="${AccountPage.id}"
                                            class="btn btn-primary">Add to
                                        friend
                                    </button>
                                </form>
                            </c:if>
                            <c:if test="${AccountPage.id == ActiveAccount.id}">
                                <form action="${pageContext.request.contextPath}/account" method="post">
                                    <a href="${pageContext.request.contextPath}/changeAccount" type="submit"
                                       name="change" value="" class="btn btn-primary">Change</a>
                                </form>
                            </c:if>
                            <form action="${pageContext.request.contextPath}/account" method="post">
                                <button type="submit" name="toXML" value="${AccountPage}" class="btn btn-primary">To XML
                                </button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</body>
</html>
