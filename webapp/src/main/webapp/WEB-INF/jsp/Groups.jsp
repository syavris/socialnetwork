<%--
  Created by IntelliJ IDEA.
  User: Даниил
  Date: 28.12.2016
  Time: 21:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="https://code.jquery.com/jquery-2.1.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<html>
<head>
    <title>Groups</title>
</head>
<body>
<jsp:include page="Menu.jsp"/>
<form action="${pageContext.request.contextPath}/groups" method="post">
    <a href="${pageContext.request.contextPath}/createGroup" type="submit" name="create" class="btn btn-primary">Create
        group</a>
</form>
<c:forEach items="${Groups}" var="group">
    <img src=${pageContext.request.contextPath}/image?photoName=${group.photo} class="img-circle" width="50"
         height="50">
    <a href="${pageContext.request.contextPath}/group?id=${group.id}">${group.name}</a>
    <form action="${pageContext.request.contextPath}/groups" method="post">
        <button type="submit" name="out" value="${group.id}" class="btn btn-primary">Out of the group</button>
    </form>
</c:forEach>
</body>
</html>
