<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Даниил
  Date: 06.01.2017
  Time: 14:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="https://code.jquery.com/jquery-2.1.4.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/phonesScript.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/confirmDialog.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <link href="${pageContext.request.contextPath}/css/mainPage.css" rel="stylesheet">
    <script>
        $(function () {
            $("#datepicker").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
    <title>Modify account</title>
</head>
<body>
<jsp:include page="Menu.jsp"/>

<div class="container">
    <form class="form-signin" action="${pageContext.request.contextPath}/changeAccount" method="post"
          enctype="multipart/form-data">

        <h2 class="form-signin-heading">Account information</h2>
        <div class="form-signin-heading" style="color:red">${errorMessage}</div>

        <div class="control-group">
            <label class="control-label" for="name">Your Name</label>
            <div class="controls">
                <input size="50" name="name" id="name" value="${EditAccount.name}" type="text" class="form-control"
                       placeholder="Enter your Name">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="password">Password</label>
            <div class="controls">
                <input size="50" name="password" id="password" value="${EditAccount.password}" type="password"
                       class="form-control"
                       placeholder="Enter your Password">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="email">Your Email</label>
            <div class="controls">
                <input size="50" name="email" id="email" value="${EditAccount.email}" type="text" class="form-control"
                       placeholder="Enter your Email" readonly="readonly">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="datepicker">Your Birthday</label>
            <div class="controls">
                <input size="50" name="dateOfBirth" id="datepicker"
                       value="<fmt:formatDate value="${EditAccount.dateOfBirth}" pattern="yyyy-MM-dd" />" type="text"
                       class="form-control"
                       placeholder="Choose your Birthday" readonly='readonly'>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Sex</label>
            <div class="cols-sm-10">
                <label class="radio-inline" for="sex-0">
                    <input type="radio" name="sex" id="sex-0" value="MALE"
                           <c:if test="${EditAccount.sex == 'MALE'}">checked="checked"</c:if> >
                    Male
                </label>
                <label class="radio-inline" for="sex-1">
                    <input type="radio" name="sex" id="sex-1" value="FEMALE"
                           <c:if test="${EditAccount.sex == 'FEMALE'}">checked="checked"</c:if> >
                    Female
                </label>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="home">Your Home Address</label>
            <div class="controls">
                <input size="50" name="homeAddress" id="home" value="${EditAccount.homeAddress}" type="text"
                       class="form-control"
                       placeholder="Enter your Home Address">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="work">Your Work Address</label>
            <div class="controls">
                <input size="50" name="workAddress" id="work" value="${EditAccount.workAddress}" type="text"
                       class="form-control"
                       placeholder="Enter your Work Address">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Your Phones</label>
            <table class="table" id="pTable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Number</th>
                    <th>Type</th>
                </tr>
                </thead>
                <tbody>
                <c:set var="count" value="0"/>
                <c:forEach items="${EditAccount.homeNumbers}" var="home">
                    <tr>
                        <td class='pNo'><c:set var="count" value="${count+1}"/>
                            <c:out value="${count}"/></td>
                        <td><input name='homeNumbers' type='text' value="${home}"></td>
                        <td><select name='numberType'>
                            <option value='Home' selected>Home</option>
                            <option value='Work'>Work</option>
                        </select></td>
                        <td>
                            <button type='button' id='rButton' class='btn btn-primary'>Remove</button>
                        </td>
                    </tr>
                </c:forEach>
                <c:forEach items="${EditAccount.workNumbers}" var="work">
                    <tr>
                        <td class='pNo'><c:set var="count" value="${count+1}"/>
                            <c:out value="${count}"/></td>
                        <td><input name='workNumbers' type='text' value="${work}"></td>
                        <td><select name='numberType'>
                            <option value='Home'>Home</option>
                            <option value='Work' selected>Work</option>
                        </select></td>
                        <td>
                            <button type='button' id='rButton' class='btn btn-primary'>Remove</button>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <button id="addButId" type="button" class="btn btn-large btn-primary btn-block">Add Phone
            </button>

            <div class="modal fade" id="addPhoneModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Error</h4>
                        </div>
                        <div class="modal-body" id="dialogBody">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="icq">Your ICQ</label>
            <div class="controls">
                <input size="50" name="ICQ" id="icq"
                       value="${EditAccount.ICQ}" type="number"
                       class="form-control"
                       placeholder="Enter your ICQ">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="skype">Your Skype</label>
            <div class="controls">
                <input size="50" name="skype" id="skype" value="${EditAccount.skype}" type="text" class="form-control"
                       placeholder="Enter your Skype">
            </div>
        </div>

        <label class="btn btn-large btn-primary btn-block">
            Upload from XML <input id="XML" type="file" name="uploadedXML" style="display: none;">
        </label>

        <script>
            $(document).ready(function () {
                $("#XML").change(function () {
                    $("#fromXML").click();
                });
            })
        </script>

        <label class="btn btn-large btn-primary btn-block">
            Choose your photo <input type="file" name="uploadedPhoto" style="display: none;">
        </label>

        <button name="register" id="confirm" value="" type="button" class="btn btn-large btn-primary btn-block">
            Change
        </button>

        <div class="modal fade" id="confirmModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure?
                    </div>
                    <div class="modal-footer">
                        <button name="change" value="" type="submit" class="btn btn-primary">Yes
                        </button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <button id="fromXML" name="fromXML" type="submit" hidden></button>
    </form>
</div>
</body>
</html>
