<%--
  Created by IntelliJ IDEA.
  User: Даниил
  Date: 28.12.2016
  Time: 20:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

<nav class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
        <a class="navbar-brand">Hello <b>${ActiveAccount.name}</b></a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li><a href="${pageContext.request.contextPath}/account?id=${ActiveAccount.id}">Main page</a></li>
            <li><a href="${pageContext.request.contextPath}/groups">Groups</a></li>
            <li><a href="${pageContext.request.contextPath}/friends">Friends</a></li>
        </ul>
        <div class="col-sm-3 col-md-3">
            <form class="navbar-form" role="search" action="${pageContext.request.contextPath}/search" method="get">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search" name="filter">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit" name="search"><i
                                class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="${pageContext.request.contextPath}/logout">Logout</a></li>
        </ul>
    </div>
</nav>

<script>
    $(document).ready(function () {
        $('input[name=filter]').autocomplete({
            source: function (request, response) {
                $.get('<c:url value="/getNames?filter=" />' + request.term, function (data) {
                    response(data);
                })
            },
            select: function (event, ui) {
                location.href = '<c:url value="/getByName?name=" />' + ui.item.value;
            }
        })
    })
</script>
