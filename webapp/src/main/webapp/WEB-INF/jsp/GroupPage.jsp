<%--
  Created by IntelliJ IDEA.
  User: Даниил
  Date: 03.01.2017
  Time: 13:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="https://code.jquery.com/jquery-2.1.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<link href="${pageContext.request.contextPath}/css/accountPage.css" rel="stylesheet">
<html>
<head>
    <title>${Group.name}</title>
</head>
<body>
<jsp:include page="Menu.jsp"/>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad">


            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">${Group.name}</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 " align="center"><img
                                src="${pageContext.servletContext.contextPath }/image?photoName=${Group.photo}"
                                class="img-circle img-responsive"></div>

                        <div class=" col-md-9 col-lg-9 ">
                            <table class="table table-user-information">
                                <tbody>
                                <tr>
                                    <td>Active members</td>
                                    <td>
                                        <c:forEach items="${activeMembers}" var="account">
                                            <a href="${pageContext.request.contextPath}/account?id=${account.id}">${account.name}</a>
                                            <c:if test="${creatorId == ActiveAccount.id}">
                                                <form action="${pageContext.request.contextPath}/group" method="post">
                                                    <button type="submit" name="delete" value="${account.id}"
                                                            class="btn btn-primary">Delete
                                                    </button>
                                                </form>
                                            </c:if>
                                            <br>
                                        </c:forEach>
                                    </td>
                                </tr>
                                <c:if test="${creatorId == ActiveAccount.id}">
                                    <tr>
                                        <td>Inactive members</td>
                                        <td>
                                            <c:forEach items="${inactiveMembers}" var="account">
                                                <a href="${pageContext.request.contextPath}/account?id=${account.id}">${account.name}</a>
                                                <c:if test="${creatorId == ActiveAccount.id}">
                                                    <form action="${pageContext.request.contextPath}/group"
                                                          method="post">
                                                        <button type="submit" name="delete" value="${account.id}"
                                                                class="btn btn-primary">Delete
                                                        </button>
                                                        <button type="submit" name="add" value="${account.id}"
                                                                class="btn btn-primary">Add to
                                                            group
                                                        </button>
                                                    </form>
                                                </c:if>
                                                <br>
                                            </c:forEach>
                                        </td>
                                    </tr>
                                </c:if>

                                </tbody>
                            </table>

                            <c:if test="${creatorId == ActiveAccount.id}">
                                <a href="${pageContext.request.contextPath}/changeGroup" type="submit" name="change"
                                   class="btn btn-primary">Change
                                </a>
                            </c:if>
                            <c:if test="${!isInGroup}">
                                <form action="${pageContext.request.contextPath}/group" method="post">
                                    <button type="submit" name="joinGroup" value="${ActiveAccount.id}"
                                            class="btn btn-primary">Join the
                                        group
                                    </button>
                                </form>
                            </c:if>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</body>
</html>
