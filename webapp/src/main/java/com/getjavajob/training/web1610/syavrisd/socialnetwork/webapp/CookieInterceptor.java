package com.getjavajob.training.web1610.syavrisd.socialnetwork.webapp;

import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Account;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.service.AccountService;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.service.AccountServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static com.getjavajob.training.web1610.syavrisd.socialnetwork.webapp.ControllersUtils.USER_IN_SESSION;

/**
 * Created by Даниил on 11.02.2017.
 */
public class CookieInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(CookieInterceptor.class);
    private final AccountService accountService;

    @Autowired
    public CookieInterceptor(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        logger.info("In CookieInterceptor class, preHandle method");
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute(USER_IN_SESSION);
        if (account == null) {
            Cookie[] cookies = request.getCookies();
            String email = null;
            String password = null;
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if ("email".equals(cookie.getName())) {
                        email = cookie.getValue();
                    }
                    if ("password".equals(cookie.getName())) {
                        password = cookie.getValue();
                    }
                }
            }
            if (email != null && password != null) {
                try {
                    account = accountService.getAccountByEmail(email);
                } catch (AccountServiceException e) {
                    logger.error("Something went wrong: " + e.getMessage(), e);
                }
                if (account != null && password.equals(account.getPassword())) {
                    session.setAttribute(USER_IN_SESSION, account);
                    logger.info("End of CookieInterceptor method");
                    return true;
                }
            }
            response.sendRedirect(request.getContextPath() + "/");
            logger.info("End of CookieInterceptor method");
            return false;
        }
        logger.info("End of CookieInterceptor method");
        return true;
    }
}
