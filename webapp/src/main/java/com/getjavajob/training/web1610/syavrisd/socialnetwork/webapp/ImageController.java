package com.getjavajob.training.web1610.syavrisd.socialnetwork.webapp;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.getjavajob.training.web1610.syavrisd.socialnetwork.webapp.ControllersUtils.*;

/**
 * Created by Даниил on 11.02.2017.
 */
@Controller
public class ImageController {

    private static final Logger logger = LoggerFactory.getLogger(ImageController.class);

    @RequestMapping(value = "/image")
    @ResponseBody
    public void getImage(@ModelAttribute("photoName") String imageName, HttpServletResponse resp) {
        logger.info("In getImage method, imageName - " + imageName);
        resp.setContentType("image/jpeg");
        BasicAWSCredentials creds = new BasicAWSCredentials(ACCESS_KEY, SECRET_ACCESS_KEY);
        AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(Regions.EU_WEST_2).withCredentials(new AWSStaticCredentialsProvider(creds)).build();
        try {
            InputStream inputStream;
            if (imageName != null && !imageName.isEmpty() && !imageName.equals("null")) {
                inputStream = s3Client.getObject(new GetObjectRequest(BUCKET_NAME, imageName)).getObjectContent();
            } else {
                inputStream = s3Client.getObject(new GetObjectRequest(BUCKET_NAME, "no-avatar.jpg")).getObjectContent();
            }
            IOUtils.copy(inputStream, resp.getOutputStream());
        } catch (IOException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        } finally {
            logger.info("End of getImage method");
        }



        /*try {
            InputStream inputStream;
            if (imageName != null && !imageName.isEmpty() && !imageName.equals("null")) {
                String readImage = PATH_TO_PHOTO + imageName;
                inputStream = new FileInputStream(new File(readImage));
            } else {
                inputStream = new FileInputStream(new File(PATH_TO_PHOTO + "no-avatar.jpg"));
            }
            IOUtils.copy(inputStream, resp.getOutputStream());
        } catch (IOException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        } finally {
            logger.info("End of getImage method");
        }*/
    }
}
