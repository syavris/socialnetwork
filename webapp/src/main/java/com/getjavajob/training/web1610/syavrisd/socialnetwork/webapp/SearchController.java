package com.getjavajob.training.web1610.syavrisd.socialnetwork.webapp;

import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Account;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Group;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.service.AccountService;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.service.AccountServiceException;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.service.GroupService;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.service.GroupServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by Даниил on 07.02.2017.
 */
@Controller
public class SearchController {

    private static final Logger logger = LoggerFactory.getLogger(SearchController.class);
    private final AccountService accountService;
    private final GroupService groupService;

    @Autowired
    public SearchController(AccountService accountService, GroupService groupService) {
        this.accountService = accountService;
        this.groupService = groupService;
    }

    @RequestMapping("/getNames")
    @ResponseBody
    public List<String> getNames(@RequestParam("filter") String filter) {
        logger.info("In getNames method, filter - " + filter);
        try {
            return accountService.getNames(filter);
        } catch (AccountServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        } finally {
            logger.info("End of getNames method");
        }
        return null;
    }

    @RequestMapping("/getByName")
    public String getByName(@RequestParam("name") String name) {
        logger.info("In getByName method, name - " + name);
        int accountId = 0;
        int groupId = 0;
        try {
            accountId = accountService.getIdByName(name);
        } catch (AccountServiceException e) {
            logger.info("Something went wrong: " + e.getMessage());
        }
        try {
            groupId = groupService.getIdByName(name);
        } catch (GroupServiceException e) {
            logger.info("Something went wrong: " + e.getMessage());
        }
        logger.info("End of getByName method");
        if ((accountId != 0 && groupId != 0) || (accountId == 0 && groupId == 0)) {
            return "redirect:/search?filter=" + name;
        }
        if (accountId != 0) {
            return "redirect:/account?id=" + accountId;
        } else {
            return "redirect:/group?id=" + groupId;
        }
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ModelAndView search(@RequestParam("filter") String filter) {
        logger.info("In search method, filter - " + filter);
        long accountListSize = 0;
        long groupListSize = 0;
        try {
            accountListSize = accountService.getCountByNamePart(filter);
            groupListSize = groupService.getCountByNamePart(filter);
        } catch (AccountServiceException | GroupServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        ModelAndView modelAndView = new ModelAndView("SearchResult");
        modelAndView.addObject("accountSize", accountListSize);
        modelAndView.addObject("groupSize", groupListSize);
        modelAndView.addObject("filter", filter);
        logger.info("End of search method");
        return modelAndView;
    }

    @RequestMapping(value = "/getAccounts")
    @ResponseBody
    public List<Account> getAccounts(@RequestParam("filter") String filter, @RequestParam("recordsToGet") int recordsToGet,
                                     @RequestParam("startIndex") int startIndex) {
        logger.info("In getAccounts method, filter - " + filter + ", startIndex - " + startIndex + ", recordsToFetch - " + recordsToGet);
        List<Account> accountList = null;
        try {
            accountList = accountService.getByNamePart(filter, startIndex, recordsToGet);
        } catch (AccountServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        logger.info("End of getAccounts method");
        return accountList;
    }

    @RequestMapping(value = "/getGroups")
    @ResponseBody
    public List<Group> getGroups(@RequestParam("filter") String filter, @RequestParam("recordsToGet") int recordsToGet,
                                 @RequestParam("startIndex") int startIndex) {
        logger.info("In getGroups method, filter - " + filter + ", startIndex - " + startIndex + ", recordsToFetch - " + recordsToGet);
        List<Group> groupList = null;
        try {
            groupList = groupService.getByNamePart(filter, startIndex, recordsToGet);
        } catch (GroupServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        logger.info("End of getGroups method");
        return groupList;
    }

    @RequestMapping(value = "/getTotalAccounts")
    @ResponseBody
    public Long getTotalAccounts(@RequestParam("filter") String filter) {
        logger.info("In getTotalAccounts method, filter - " + filter);
        try {
            logger.info("End of getTotalAccounts method");
            return accountService.getCountByNamePart(filter);
        } catch (AccountServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        logger.info("End of getTotalAccounts method");
        return 0L;
    }

    @RequestMapping(value = "/getTotalGroups")
    @ResponseBody
    public Long getTotalGroups(@RequestParam("filter") String filter) {
        logger.info("In getTotalGroups method, filter - " + filter);
        try {
            return groupService.getCountByNamePart(filter);
        } catch (GroupServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        logger.info("End of getTotalGroups method");
        return 0L;
    }
}
