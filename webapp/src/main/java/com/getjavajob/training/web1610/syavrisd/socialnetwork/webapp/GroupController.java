package com.getjavajob.training.web1610.syavrisd.socialnetwork.webapp;

import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Account;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Group;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.service.AccountService;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.service.AccountServiceException;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.service.GroupService;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.service.GroupServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.util.List;

import static com.getjavajob.training.web1610.syavrisd.socialnetwork.webapp.ControllersUtils.*;

/**
 * Created by Даниил on 07.02.2017.
 */
@Controller
public class GroupController {

    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);
    private final GroupService groupService;
    private final AccountService accountService;

    @Autowired
    public GroupController(GroupService groupService, AccountService accountService) {
        this.groupService = groupService;
        this.accountService = accountService;
    }

    @RequestMapping(value = "/groups", method = RequestMethod.POST)
    public String outOfGroup(@RequestParam("out") int id, HttpSession httpSession) {
        logger.info("In outOfGroup method, group id - " + id);
        try {
            Group group = groupService.getGroupById(id);
            groupService.deleteMember(group, (Account) httpSession.getAttribute(USER_IN_SESSION));
        } catch (GroupServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        logger.info("End of outOfGroup method");
        return "redirect:/groups";
    }

    @RequestMapping(value = "/createGroup", method = RequestMethod.GET)
    public ModelAndView creationGroupPage() {
        return new ModelAndView("CreateGroup");
    }

    @RequestMapping(value = "/createGroup", params = "create", method = RequestMethod.POST)
    public String createGroup(@ModelAttribute Group group, @RequestParam("uploadedPhoto") MultipartFile file,
                              HttpSession httpSession, RedirectAttributes attr) {
        logger.info("In createGroup method, group - " + group.toString());
        Group checkGroup = null;
        try {
            checkGroup = groupService.getGroupByName(group.getName());
        } catch (GroupServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        if (checkGroup != null) {
            attr.addFlashAttribute("errorMessage", "Group name already exists");
            logger.info("End of createGroup method, name already exists");
            return "redirect:/createGroup";
        }
        group.setPhoto(readUploadedPhoto(group.getName(), file));
        group.setCreatorAccount((Account) httpSession.getAttribute(USER_IN_SESSION));
        try {
            groupService.createGroup(group);
        } catch (GroupServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        logger.info("End of createGroup method");
        return "redirect:/groups";
    }

    @RequestMapping(value = "/group", method = RequestMethod.GET)
    public ModelAndView showGroup(@RequestParam("id") int id, HttpSession httpSession) {
        logger.info("In showGroup method, group id - " + id);
        int creatorId = 0;
        Group group = null;
        Account account = (Account) httpSession.getAttribute(USER_IN_SESSION);
        List<Account> activeMembers = null;
        List<Account> inactiveMembers = null;
        boolean isInGroup = false;
        try {
            group = groupService.getGroupById(id);
            if (group.getCreatorAccount() != null) {
                creatorId = group.getCreatorAccount().getId();
            } else {
                creatorId = 0;
            }
            activeMembers = groupService.getActiveMembers(group);
            inactiveMembers = groupService.getInActiveMembers(group);
            isInGroup = groupService.isAccountInGroup(group, account);
        } catch (GroupServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        ModelAndView modelAndView = new ModelAndView("GroupPage");
        modelAndView.addObject(GROUP_PAGE, group);
        modelAndView.addObject("creatorId", creatorId);
        modelAndView.addObject("activeMembers", activeMembers);
        modelAndView.addObject("inactiveMembers", inactiveMembers);
        modelAndView.addObject("isInGroup", isInGroup);
        httpSession.setAttribute(GROUP_PAGE, group);
        logger.info("End of showGroup method");
        return modelAndView;
    }

    @RequestMapping(value = "/group", params = "delete", method = RequestMethod.POST)
    public String deleteFromGroup(@RequestParam("delete") int accountId, HttpSession httpSession) {
        logger.info("In deleteFromGroup method, id account to delete - " + accountId);
        Group group = (Group) httpSession.getAttribute(GROUP_PAGE);
        try {
            Account account = accountService.getAccountById(accountId);
            groupService.deleteMember(group, account);
        } catch (GroupServiceException | AccountServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        logger.info("End of deleteFromGroup method");
        return "redirect:/group?id=" + group.getId();
    }

    @RequestMapping(value = "/group", params = "add", method = RequestMethod.POST)
    public String addToGroup(@RequestParam("add") int accountId, HttpSession httpSession) {
        logger.info("In addToGroup method, id account to add - " + accountId);
        Group group = (Group) httpSession.getAttribute(GROUP_PAGE);
        try {
            Account account = accountService.getAccountById(accountId);
            groupService.acceptMember(group, account);
        } catch (GroupServiceException | AccountServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        logger.info("End of addToGroup method");
        return "redirect:/group?id=" + group.getId();
    }

    @RequestMapping(value = "/group", params = "joinGroup", method = RequestMethod.POST)
    public String joinGroup(HttpSession httpSession) {
        logger.info("In joinGroup method");
        Account account = (Account) httpSession.getAttribute(USER_IN_SESSION);
        Group group = (Group) httpSession.getAttribute(GROUP_PAGE);
        try {
            groupService.addAccountToGroup(group, account);
        } catch (GroupServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        logger.info("End of joinGroup method");
        return "redirect:/group?id=" + group.getId();
    }

    @RequestMapping(value = "/changeGroup", method = RequestMethod.GET)
    public ModelAndView changeGroupPage() {
        return new ModelAndView("ChangeGroup");
    }

    @RequestMapping(value = "/changeGroup", method = RequestMethod.POST)
    public String changeGroup(@ModelAttribute Group group, @RequestParam("uploadedPhoto") MultipartFile file,
                              HttpSession httpSession) {
        logger.info("In changeGroup method, group - " + group.toString());
        Group activeGroup = (Group) httpSession.getAttribute(GROUP_PAGE);
        if (!file.isEmpty()) {
            group.setPhoto(readUploadedPhoto(group.getName(), file));
        } else {
            group.setPhoto(activeGroup.getPhoto());
        }
        group.setCreatorAccount(activeGroup.getCreatorAccount());
        group.setId(activeGroup.getId());
        try {
            groupService.updateGroup(group);
        } catch (GroupServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        httpSession.setAttribute(GROUP_PAGE, group);
        logger.info("End of changeGroup method");
        return "redirect:/group?id=" + activeGroup.getId();
    }

}
