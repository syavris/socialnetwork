package com.getjavajob.training.web1610.syavrisd.socialnetwork.webapp;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * Created by Даниил on 10.02.2017.
 */
class ControllersUtils {

    static final String PATH_TO_PHOTO = "D:\\dev\\photoForProject\\";
    static final String USER_IN_SESSION = "ActiveAccount";
    static final String USER_PAGE = "AccountPage";
    static final String GROUP_PAGE = "Group";
    private static final Logger logger = LoggerFactory.getLogger(ControllersUtils.class);
    static final String ACCESS_KEY = "AKIAIMWYNLBABSVKYQCQ";
    static final String SECRET_ACCESS_KEY = "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";
    static final String BUCKET_NAME = "dssocialnetwork";


    static String readUploadedPhoto(String entityName, MultipartFile file) {
        BasicAWSCredentials creds = new BasicAWSCredentials(ACCESS_KEY, SECRET_ACCESS_KEY);
        AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(Regions.EU_WEST_2).withCredentials(new AWSStaticCredentialsProvider(creds)).build();
        String photoName = file.getOriginalFilename();
        String photoNameSave = entityName + photoName;
        try (InputStream inputStream = file.getInputStream()) {
            if (inputStream.available() > 0) {
                s3Client.putObject(new PutObjectRequest(BUCKET_NAME, photoNameSave, inputStream, new ObjectMetadata()).withCannedAcl(CannedAccessControlList.PublicRead));
            }
        } catch (IOException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        if (photoName != null && !photoName.isEmpty()) {
            logger.info("End of readUploadedPhoto method");
            return photoNameSave;
        } else {
            logger.info("End of readUploadedPhoto method");
            return null;
        }
        /*logger.info("In readUploadedPhoto method, entityName - " + entityName);
        String photoName = file.getOriginalFilename();
        try (InputStream inputStream = file.getInputStream()) {
            if (inputStream.available() > 0) {
                String photoNameSave = PATH_TO_PHOTO + entityName + photoName;
                savePhoto(inputStream, photoNameSave);
            }
        } catch (IOException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        if (photoName != null && !photoName.isEmpty()) {
            logger.info("End of readUploadedPhoto method");
            return entityName + photoName;
        } else {
            logger.info("End of readUploadedPhoto method");
            return null;
        }*/
    }

    private static void savePhoto(InputStream inputStream, String name) {
        logger.info("In savePhoto method, destination - " + name);
        Path destination = Paths.get(name);
        try {
            Files.copy(inputStream, destination, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        } finally {
            logger.info("End of savePhoto method");
        }
    }
}
