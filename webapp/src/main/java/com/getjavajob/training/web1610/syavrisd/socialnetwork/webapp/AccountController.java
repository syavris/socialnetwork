package com.getjavajob.training.web1610.syavrisd.socialnetwork.webapp;

import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Account;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Group;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.service.AccountService;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.service.AccountServiceException;
import com.thoughtworks.xstream.XStream;
import org.apache.commons.io.IOUtils;
import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.getjavajob.training.web1610.syavrisd.socialnetwork.webapp.ControllersUtils.*;

/**
 * Created by Даниил on 06.02.2017.
 */
@Controller
public class AccountController {

    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView mainPage() {
        return new ModelAndView("index");
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ModelAndView registrationPage() {
        return new ModelAndView("Registration");
    }

    @RequestMapping(value = "/registration", params = "create", method = RequestMethod.POST)
    public String createAccount(@ModelAttribute Account account, @RequestParam("uploadedPhoto") MultipartFile file,
                                @RequestParam("password") String password, RedirectAttributes attr) {
        logger.info("In createAccount method: " + account.toString());
        Account checkAccount = null;
        try {
            checkAccount = accountService.getAccountByEmail(account.getEmail());
        } catch (AccountServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        if (checkAccount != null) {
            attr.addFlashAttribute("errorMessage", "Email already exists");
            logger.info("End of createAccount method, email already exists");
            return "redirect:/registration";
        }
        account.setPhoto(readUploadedPhoto(account.getName(), file));
        account.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
        try {
            accountService.createAccount(account);
        } catch (AccountServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        logger.info("End of createAccount method");
        return "redirect:/";
    }

    @RequestMapping(value = "/", params = "enter", method = RequestMethod.POST)
    public String enter(@RequestParam("email") String email, @RequestParam("password") String password,
                        @RequestParam(value = "rememberMe", required = false) String remember,
                        HttpSession httpSession, RedirectAttributes attr, HttpServletResponse response) {
        logger.info("In enter method. Email - " + email + ", password - " + password);
        Account account = null;
        try {
            account = accountService.getAccountByEmail(email);
        } catch (AccountServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        if (account == null || !BCrypt.checkpw(password, account.getPassword())) {
            attr.addFlashAttribute("errorMessage", "Account doesn't exists");
            logger.info("End of enter method, account doesn't exists");
            return "redirect:/";
        }
        if (remember != null) {
            logger.info("RememberMe not empty, creating cookies");
            Cookie cookieEmail = new Cookie("email", account.getEmail());
            Cookie cookiePass = new Cookie("password", account.getPassword());
            cookieEmail.setMaxAge(24 * 60 * 60);
            cookiePass.setMaxAge(24 * 60 * 60);
            response.addCookie(cookieEmail);
            response.addCookie(cookiePass);
            logger.info("End of creating cookies");
        }
        httpSession.setAttribute(USER_IN_SESSION, account);
        logger.info("End of enter method");
        return "redirect:/account?id=" + account.getId();
    }

    @RequestMapping(value = "/account", method = RequestMethod.GET)
    public ModelAndView showAccountPage(@RequestParam("id") int id, HttpSession httpSession) {
        logger.info("In showAccountPage method, id - " + id);
        Account account = null;
        boolean isFriend = false;
        try {
            account = accountService.getAccountById(id);
            isFriend = accountService.isFriend((Account) httpSession.getAttribute(USER_IN_SESSION), account);
        } catch (AccountServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        ModelAndView modelAndView = new ModelAndView(USER_PAGE);
        modelAndView.addObject("isFriend", isFriend);
        httpSession.setAttribute(USER_PAGE, account);
        logger.info("End of showAccountPage method");
        return modelAndView;
    }

    @RequestMapping(value = "/account", params = "addToFriend", method = RequestMethod.POST)
    public String addToFriend(@RequestParam("addToFriend") int id, HttpSession httpSession) {
        logger.info("In addToFriend method, friend id - " + id);
        Account userPage = null;
        try {
            userPage = accountService.getAccountById(id);
        } catch (AccountServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        Account activeAccount = (Account) httpSession.getAttribute(USER_IN_SESSION);
        try {
            accountService.addFriendToAccount(activeAccount, userPage);
        } catch (AccountServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        logger.info("End of addToFriend method");
        return "redirect:/account?id=" + userPage.getId();
    }

    @RequestMapping(value = "/account", params = "toXML", method = RequestMethod.POST, produces = "application/xml")
    public void toXML(HttpSession httpSession, HttpServletResponse httpServletResponse) {
        logger.info("In toXML method");
        Account account = (Account) httpSession.getAttribute(USER_PAGE);
        XStream xStream = new XStream();
        xStream.processAnnotations(Account.class);
        xStream.addDefaultImplementation(java.sql.Timestamp.class, Date.class);
        xStream.registerConverter(new AccountDateConverter());
        String xml = xStream.toXML(account);
        httpServletResponse.setHeader("Content-Disposition", "attachment; filename=" + account.getName() + ".xml");
        try {
            IOUtils.copy(new ByteArrayInputStream(xml.getBytes()), httpServletResponse.getOutputStream());
        } catch (IOException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        logger.info("End of toXML method");
    }

    @RequestMapping(value = "/changeAccount", params = "fromXML", method = RequestMethod.POST)
    public ModelAndView fromXML(@RequestParam("uploadedXML") MultipartFile file, HttpSession httpSession) {
        logger.info("In fromXML method");
        XStream xStream = new XStream();
        xStream.processAnnotations(Account.class);
        xStream.registerConverter(new AccountDateConverter());
        String xml = null;
        ModelAndView modelAndView = new ModelAndView("ChangeAccount");
        try {
            xml = IOUtils.toString(new ByteArrayInputStream(file.getBytes()));
        } catch (IOException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        Account account;
        try {
            account = (Account) xStream.fromXML(xml);
        } catch (Exception e) {
            modelAndView.addObject("errorMessage", "Wrong document type");
            modelAndView.addObject("EditAccount", httpSession.getAttribute(USER_IN_SESSION));
            logger.info("End of fromXML method");
            return modelAndView;
        }
        account.setPassword(((Account) httpSession.getAttribute(USER_IN_SESSION)).getPassword());
        modelAndView.addObject("EditAccount", account);
        logger.info("End of fromXML method");
        return modelAndView;
    }

    @RequestMapping(value = "/changeAccount", method = RequestMethod.GET)
    public ModelAndView viewChangeAccount(HttpSession httpSession) {
        ModelAndView modelAndView = new ModelAndView("ChangeAccount");
        modelAndView.addObject("EditAccount", httpSession.getAttribute(USER_IN_SESSION));
        return modelAndView;
    }

    @RequestMapping(value = "/changeAccount", params = "change", method = RequestMethod.POST)
    public String changeAccount(@ModelAttribute Account account, @RequestParam("uploadedPhoto") MultipartFile file,
                                HttpSession httpSession, HttpServletResponse response) {
        logger.info("In changeAccount method, account - " + account.toString());
        Account activeAccount = (Account) httpSession.getAttribute(USER_IN_SESSION);
        if (!file.isEmpty()) {
            account.setPhoto(readUploadedPhoto(account.getName(), file));
        } else {
            account.setPhoto(activeAccount.getPhoto());
        }
        String newPassword = account.getPassword();
        String oldPassword = activeAccount.getPassword();
        if (!newPassword.equals(oldPassword)) {
            account.setPassword(BCrypt.hashpw(newPassword, BCrypt.gensalt()));
            Cookie cookiePass = new Cookie("password", account.getPassword());
            cookiePass.setMaxAge(24 * 60 * 60);
            response.addCookie(cookiePass);
        }
        account.setId(activeAccount.getId());
        try {
            accountService.updateAccount(account);
        } catch (AccountServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        httpSession.setAttribute(USER_IN_SESSION, account);
        logger.info("End of changeAccount method");
        return "redirect:/account?id=" + activeAccount.getId();
    }


    @RequestMapping(value = "/friends", method = RequestMethod.GET)
    public ModelAndView showFriends(HttpSession httpSession) {
        logger.info("In showFriends method");
        Account account = (Account) httpSession.getAttribute(USER_IN_SESSION);
        List<Account> active = null;
        List<Account> inactive = null;
        try {
            active = accountService.getActiveFriends(account);
            inactive = accountService.getInActiveFriends(account);
        } catch (AccountServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        ModelAndView modelAndView = new ModelAndView("Friends");
        modelAndView.addObject("Active", active);
        modelAndView.addObject("Inactive", inactive);
        logger.info("End of showFriends method");
        return modelAndView;
    }

    @RequestMapping(value = "/friends", params = "delete", method = RequestMethod.POST)
    public String deleteFriend(@RequestParam("delete") int id, HttpSession httpSession) {
        logger.info("In deleteFriend method, id friend to delete - " + id);
        Account owner = (Account) httpSession.getAttribute(USER_IN_SESSION);
        try {
            Account friend = accountService.getAccountById(id);
            accountService.deleteFriend(friend, owner);
            accountService.deleteFriend(owner, friend);
        } catch (AccountServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        logger.info("End of deleteFriend method");
        return "redirect:/friends";
    }

    @RequestMapping(value = "/friends", params = "add", method = RequestMethod.POST)
    public String confirmFriend(@RequestParam("add") int id, HttpSession httpSession) {
        logger.info("In confirmFriend method, id friend to confirm - " + id);
        Account owner = (Account) httpSession.getAttribute(USER_IN_SESSION);
        try {
            Account friend = accountService.getAccountById(id);
            accountService.acceptFriend(friend, owner);
        } catch (AccountServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        logger.info("End of confirmFriend method");
        return "redirect:/friends";
    }

    @RequestMapping(value = "/groups", method = RequestMethod.GET)
    public ModelAndView showGroups(HttpSession httpSession) {
        logger.info("In showGroups method");
        Account account = (Account) httpSession.getAttribute(USER_IN_SESSION);
        List<Group> list = null;
        try {
            list = accountService.getGroups(account);
        } catch (AccountServiceException e) {
            logger.error("Something went wrong: " + e.getMessage(), e);
        }
        ModelAndView modelAndView = new ModelAndView("Groups");
        modelAndView.addObject("Groups", list);
        logger.info("End of showGroups method");
        return modelAndView;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpSession httpSession, HttpServletResponse response) {
        logger.info("In logout method");
        Cookie cookieEmail = new Cookie("email", null);
        cookieEmail.setMaxAge(0);
        Cookie cookiePassword = new Cookie("password", null);
        cookiePassword.setMaxAge(0);
        response.addCookie(cookieEmail);
        response.addCookie(cookiePassword);
        if (httpSession != null) {
            httpSession.invalidate();
        }
        logger.info("End of logout method");
        return "redirect:/";
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
}
