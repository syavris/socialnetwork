CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `sex` varchar(6) DEFAULT NULL,
  `work_address` varchar(255) DEFAULT NULL,
  `home_address` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `skype` varchar(255) DEFAULT NULL,
  `ICQ` int(11) DEFAULT NULL,
  `photo` varchar(250) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Account_email_uindex` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8;

CREATE TABLE `home_numbers` (
  `id` int(11) DEFAULT NULL,
  `number` varchar(12) DEFAULT NULL,
  KEY `home_numbers_account_id_fk` (`id`),
  CONSTRAINT `home_numbers_account_id_fk` FOREIGN KEY (`id`) REFERENCES `account` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `work_numbers` (
  `id` int(11) DEFAULT NULL,
  `number` varchar(12) DEFAULT NULL,
  KEY `work_numbers_account_id_fk` (`id`),
  CONSTRAINT `work_numbers_account_id_fk` FOREIGN KEY (`id`) REFERENCES `account` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `friends` (
  `first_account_id` int(11) DEFAULT NULL,
  `second_account_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  KEY `friends_account_id_fk` (`first_account_id`),
  KEY `friends1_account_id_fk` (`second_account_id`),
  CONSTRAINT `friends1_account_id_fk` FOREIGN KEY (`second_account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `friends_account_id_fk` FOREIGN KEY (`first_account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_name_uindex` (`name`),
  KEY `groups_account_id_fk` (`creator_id`),
  CONSTRAINT `groups_account_id_fk` FOREIGN KEY (`creator_id`) REFERENCES `account` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

CREATE TABLE `accounts_in_groups` (
  `group_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  KEY `accounts_in_group_group_id_fk` (`group_id`),
  KEY `accounts_in_group_account_id_fk` (`account_id`),
  CONSTRAINT `accounts_in_group_account_id_fk` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `accounts_in_group_group_id_fk` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8