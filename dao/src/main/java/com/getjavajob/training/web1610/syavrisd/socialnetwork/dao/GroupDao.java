package com.getjavajob.training.web1610.syavrisd.socialnetwork.dao;

import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Account;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Group;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Participant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Даниил on 05.12.2016.
 */
@Repository
public class GroupDao extends AbstractDao<Group> {

    private static final Logger logger = LoggerFactory.getLogger(GroupDao.class);

    public GroupDao() {
    }

    @Override
    protected Class<Group> getClassType() {
        return Group.class;
    }

    public boolean update(final Group group) throws DaoException {
        logger.info("In update method, group - " + group.toString());
        try {
            Group groupFromDB = entityManager.find(getClassType(), group.getId());
            if (groupFromDB == null) {
                logger.info("End of update method, false");
                return false;
            }
            group.setMembers(groupFromDB.getMembers());
            entityManager.merge(group);
            logger.info("End of update method, true");
            return true;
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public boolean create(Group group) throws DaoException {
        logger.info("In create method, group - " + group.toString());
        if (super.create(group)) {
            addAccountToGroupOrFriend(group.getId(), group.getCreatorAccount().getId());
            acceptFriendOrMember(group.getId(), group.getCreatorAccount().getId());
            logger.info("End of create method, true");
            return true;
        } else {
            logger.info("End of create method, false");
            return false;
        }
    }

    public boolean isAccountInGroup(int groupId, int accountId) throws DaoException {
        logger.info("In isAccountInGroup method, groupId - " + groupId + ", accountId - " + accountId);
        try {
            Group group = entityManager.find(getClassType(), groupId);
            Account member = entityManager.find(Account.class, accountId);
            if (group == null || member == null) {
                logger.info("End of isAccountInGroup method, false");
                return false;
            }
            for (Participant participant : group.getMembers()) {
                if (participant.getAccount().equals(member)) {
                    logger.info("End of isAccountInGroup method, true");
                    return true;
                }
            }
            logger.info("End of isAccountInGroup method, false");
            return false;
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }

    public boolean addAccountToGroupOrFriend(final int firstId, final int needToAdd) throws DaoException {
        logger.info("In addAccountToGroupOrFriend method, firstId - " + firstId + ", needToAdd - " + needToAdd);
        try {
            Group group = entityManager.find(getClassType(), firstId);
            Account participant = entityManager.find(Account.class, needToAdd);
            if (group == null || participant == null) {
                logger.info("End of addAccountToGroupOrFriend method, false");
                return false;
            }
            group.getMembers().add(new Participant(group, participant, false));
            entityManager.merge(group);
            logger.info("End of addAccountToGroupOrFriend method, true");
            return true;
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }

    private List<Account> getMembers(int entityId, boolean isActive) throws DaoException {
        logger.info("In getMembers method, entityId - " + entityId + ", isActive - " + isActive);
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Participant> criteriaQuery = criteriaBuilder.createQuery(Participant.class);
            Root<Participant> root = criteriaQuery.from(Participant.class);
            CriteriaQuery<Participant> inActive;
            if (isActive) {
                inActive = criteriaQuery.select(root).where(criteriaBuilder.and(
                        criteriaBuilder.isTrue(root.<Boolean>get("is_active")),
                        criteriaBuilder.equal(root.get("group"), entityId))
                );
            } else {
                inActive = criteriaQuery.select(root).where(criteriaBuilder.and(
                        criteriaBuilder.isFalse(root.<Boolean>get("is_active")),
                        criteriaBuilder.equal(root.get("group"), entityId))
                );
            }
            List<Participant> members = entityManager.createQuery(inActive).getResultList();
            List<Account> groupInactiveMembers = new ArrayList<>();
            for (Participant participant : members) {
                groupInactiveMembers.add(participant.getAccount());
            }
            logger.info("End of getMembers method");
            return groupInactiveMembers;
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }

    public List<Account> getInActiveFriendsOrMembers(int entityId) throws DaoException {
        return getMembers(entityId, false);
    }

    public List<Account> getActiveFriendsOrMembers(int entityId) throws DaoException {
        return getMembers(entityId, true);
    }

    public boolean acceptFriendOrMember(int firstId, int secondId) throws DaoException {
        logger.info("In acceptFriendOrMember method, firstId - " + firstId + ", secondId - " + secondId);
        try {
            Group group = entityManager.find(getClassType(), firstId);
            Account member = entityManager.find(Account.class, secondId);
            if (group == null || member == null) {
                logger.info("End of acceptFriendOrMember method, false");
                return false;
            }
            for (Participant participant : group.getMembers()) {
                if (participant.getAccount().equals(member)) {
                    participant.setIs_active(true);
                }
            }
            entityManager.merge(group);
            logger.info("End of acceptFriendOrMember method, true");
            return true;
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }

    public boolean deleteFromGroupOrFriend(int firstId, int needToDelete) throws DaoException {
        logger.info("In deleteFromGroupOrFriend method, firstId - " + firstId + ", needToDelete - " + needToDelete);
        try {
            Group group = entityManager.find(getClassType(), firstId);
            Account member = entityManager.find(Account.class, needToDelete);
            if (group == null || member == null) {
                logger.info("End of deleteFromGroupOrFriend method, false");
                return false;
            }
            for (Participant participant : group.getMembers()) {
                if (participant.getAccount().equals(member)) {
                    group.getMembers().remove(participant);
                    entityManager.merge(group);
                    logger.info("End of deleteFromGroupOrFriend method, true");
                    return true;
                }
            }
            logger.info("End of deleteFromGroupOrFriend method, false");
            return false;
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }

    public Group getByName(String name) throws DaoException {
        logger.info("In getByName method, name - " + name);
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Group> criteriaQuery = criteriaBuilder.createQuery(Group.class);
            Root<Group> root = criteriaQuery.from(Group.class);
            CriteriaQuery<Group> byName = criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("name"), name));
            logger.info("End of getByName method");
            return entityManager.createQuery(byName).getSingleResult();
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }
}
