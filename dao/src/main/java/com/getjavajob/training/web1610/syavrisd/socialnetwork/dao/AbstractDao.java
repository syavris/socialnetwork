package com.getjavajob.training.web1610.syavrisd.socialnetwork.dao;

import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Account;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Entity;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Group;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Даниил on 04.12.2016.
 */
@Repository
public abstract class AbstractDao<E extends Entity> {

    private static final Logger logger = LoggerFactory.getLogger(AbstractDao.class);
    @PersistenceContext
    protected EntityManager entityManager;

    public AbstractDao() {
    }

    protected abstract Class<E> getClassType();

    public boolean create(final E entity) throws DaoException {
        logger.info("In create method, entity - " + entity.toString());
        try {
            if (entityManager.find(getClassType(), entity.getId()) != null) {
                logger.info("End of create method, false");
                return false;
            }
            entityManager.persist(entity);
            logger.info("End of create method, true");
            return true;
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }

    public boolean delete(int id) throws DaoException {
        logger.info("In delete method, id - " + id);
        try {
            E entity = entityManager.find(getClassType(), id);
            if (entity == null) {
                logger.info("End of delete method, false");
                return false;
            }
            entityManager.remove(entity);
            logger.info("End of delete method, true");
            return true;
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }

    public E get(final int id) throws DaoException {
        logger.info("In get method, id - " + id);
        try {
            logger.info("End of get method");
            return entityManager.find(getClassType(), id);
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }

    public List<E> getAll() throws DaoException {
        logger.info("In getAll method");
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<E> criteriaQuery = criteriaBuilder.createQuery(getClassType());
            Root<E> root = criteriaQuery.from(getClassType());
            CriteriaQuery<E> selectAll = criteriaQuery.select(root);
            logger.info("End of getAll method");
            return entityManager.createQuery(selectAll).getResultList();
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }

    public List<String> getAllNames(String filter) throws DaoException {
        logger.info("In getAllNames method, filter - " + filter);
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Account> accountQuery = criteriaBuilder.createQuery(Account.class);
            Root<Account> accountRoot = accountQuery.from(Account.class);
            CriteriaQuery<Account> accountsByName = accountQuery.select(accountRoot).where(criteriaBuilder.like(
                    criteriaBuilder.lower(accountRoot.<String>get("name")), "%" + filter.toLowerCase() + "%"));
            List<Account> accountList = entityManager.createQuery(accountsByName).getResultList();
            CriteriaQuery<Group> groupQuery = criteriaBuilder.createQuery(Group.class);
            Root<Group> groupRoot = groupQuery.from(Group.class);
            CriteriaQuery<Group> groupsByName = groupQuery.select(groupRoot).where(criteriaBuilder.like(
                    criteriaBuilder.lower(groupRoot.<String>get("name")), "%" + filter.toLowerCase() + "%"));
            List<Group> groupList = entityManager.createQuery(groupsByName).getResultList();
            ArrayList<String> resultList = new ArrayList<>();
            for (Account account : accountList) {
                resultList.add(account.getName());
            }
            for (Group group : groupList) {
                resultList.add(group.getName());
            }
            logger.info("End of getAllNames method");
            return resultList;
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }

    public int getIdByName(String name) throws DaoException {
        logger.info("In getIdByName method, name - " + name);
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<E> criteriaQuery = criteriaBuilder.createQuery(getClassType());
            Root<E> root = criteriaQuery.from(getClassType());
            CriteriaQuery<E> byName = criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("name"), name));
            E entity = entityManager.createQuery(byName).getSingleResult();
            if (entity != null) {
                logger.info("End of getIdByName method, entity not null");
                return entity.getId();
            } else {
                logger.info("End of getIdByName method, entity is null");
                return 0;
            }
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }

    public List<E> getByNamePart(String filter, int from, int to) throws DaoException {
        logger.info("In getByNamePart method, filter - " + filter);
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<E> criteriaQuery = criteriaBuilder.createQuery(getClassType());
            Root<E> root = criteriaQuery.from(getClassType());
            CriteriaQuery<E> where = criteriaQuery.select(root).where(criteriaBuilder.like(
                    criteriaBuilder.lower(root.<String>get("name")), "%" + filter.toLowerCase() + "%"));
            logger.info("End of getByNamePart method");
            return entityManager.createQuery(where).setFirstResult(from).setMaxResults(to).getResultList();
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }

    public Long getCountByNamePart(String filter) throws DaoException {
        logger.info("In getCountByNamePart method, filter - " + filter);
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
            Root<E> root = criteriaQuery.from(getClassType());
            CriteriaQuery<Long> where = criteriaQuery.select(criteriaBuilder.count(root)).where(criteriaBuilder.like(
                    criteriaBuilder.lower(root.<String>get("name")), "%" + filter.toLowerCase() + "%"));
            logger.info("End of getCountByNamePart method");
            return entityManager.createQuery(where).getSingleResult();
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }
}

