package com.getjavajob.training.web1610.syavrisd.socialnetwork.dao;

/**
 * Created by Даниил on 11.12.2016.
 */
public class DaoException extends Exception {

    public DaoException(Throwable cause) {
        super(cause);
    }
}
