package com.getjavajob.training.web1610.syavrisd.socialnetwork.dao;

import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Account;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Friend;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Group;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Participant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Даниил on 05.12.2016.
 */
@Repository
public class AccountDao extends AbstractDao<Account> {

    private static final Logger logger = LoggerFactory.getLogger(AccountDao.class);

    public AccountDao() {
    }

    @Override
    protected Class<Account> getClassType() {
        return Account.class;
    }

    public boolean update(final Account account) throws DaoException {
        logger.info("In update method, account - " + account.toString());
        try {
            Account accountFromDB = entityManager.find(getClassType(), account.getId());
            if (accountFromDB == null) {
                logger.info("End of update method, false");
                return false;
            }
            account.setFriends(accountFromDB.getFriends());
            entityManager.merge(account);
            logger.info("End of update method, true");
            return true;
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }

    public Account getByEmail(String email) throws DaoException {
        logger.info("In getByEmail method, email - " + email);
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
            Root<Account> root = criteriaQuery.from(Account.class);
            CriteriaQuery<Account> byName = criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("email"), email));
            logger.info("End of getByEmail method");
            return entityManager.createQuery(byName).getSingleResult();
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }

    public boolean addAccountToGroupOrFriend(final int firstId, final int needToAdd) throws DaoException {
        logger.info("In addAccountToGroupOrFriend method, firstId - " + firstId + ", needToAdd id - " + needToAdd);
        try {
            Account account = entityManager.find(getClassType(), firstId);
            Account friend = entityManager.find(getClassType(), needToAdd);
            if (account == null || friend == null) {
                logger.info("End of addAccountToGroupOrFriend method, false");
                return false;
            }
            account.getFriends().add(new Friend(account, friend, false));
            entityManager.merge(account);
            logger.info("End of addAccountToGroupOrFriend method, true");
            return true;
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }

    private List<Account> getFriends(int entityId, boolean isActive) throws DaoException {
        logger.info("In getFriends method, entityId - " + entityId + ", isActive - " + isActive);
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Friend> criteriaQuery = criteriaBuilder.createQuery(Friend.class);
            Root<Friend> root = criteriaQuery.from(Friend.class);
            CriteriaQuery<Friend> inActive;
            if (isActive) {
                inActive = criteriaQuery.select(root).where(criteriaBuilder.and(
                        criteriaBuilder.isTrue(root.<Boolean>get("is_active")),
                        criteriaBuilder.equal(root.get("second_account"), entityId))
                );
            } else {
                inActive = criteriaQuery.select(root).where(criteriaBuilder.and(
                        criteriaBuilder.isFalse(root.<Boolean>get("is_active")),
                        criteriaBuilder.equal(root.get("second_account"), entityId))
                );
            }
            List<Friend> friends = entityManager.createQuery(inActive).getResultList();
            List<Account> accountInactiveFriends = new ArrayList<>();
            for (Friend friend : friends) {
                accountInactiveFriends.add(friend.getFirst_account());
            }
            logger.info("End of getFriends method");
            return accountInactiveFriends;
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }

    public List<Account> getInActiveFriendsOrMembers(int entityId) throws DaoException {
        return getFriends(entityId, false);
    }

    public List<Account> getActiveFriendsOrMembers(int entityId) throws DaoException {
        return getFriends(entityId, true);
    }

    public boolean acceptFriendOrMember(int firstId, int secondId) throws DaoException {
        logger.info("In acceptFriendOrMember method, firstId - " + firstId + ", secondId - " + secondId);
        try {
            Account account = entityManager.find(getClassType(), firstId);
            Account friend = entityManager.find(getClassType(), secondId);
            if (account == null || friend == null) {
                logger.info("End of acceptFriendOrMember method, false");
                return false;
            }
            for (Friend accountFriend : account.getFriends()) {
                if (accountFriend.getSecond_account().equals(friend)) {
                    accountFriend.setIs_active(true);
                }
            }
            friend.getFriends().add(new Friend(friend, account, true));
            entityManager.merge(account);
            entityManager.merge(friend);
            logger.info("End of acceptFriendOrMember method, true");
            return true;
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }

    public boolean deleteFromGroupOrFriend(int firstId, int needToDelete) throws DaoException {
        logger.info("In deleteFromGroupOrFriend method, firstId - " + firstId + ", needToDelete - " + needToDelete);
        try {
            Account account = entityManager.find(getClassType(), firstId);
            Account friend = entityManager.find(getClassType(), needToDelete);
            if (account == null || friend == null) {
                logger.info("End of deleteFromGroupOrFriend method, false");
                return false;
            }
            for (Friend accountFriend : account.getFriends()) {
                if (accountFriend.getSecond_account().equals(friend)) {
                    account.getFriends().remove(accountFriend);
                    entityManager.merge(account);
                    logger.info("End of deleteFromGroupOrFriend method, true");
                    return true;
                }
            }
            logger.info("End of deleteFromGroupOrFriend method, false");
            return false;
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }

    public boolean isFriend(int firstId, int secondId) throws DaoException {
        logger.info("In isFriend method, firstId - " + firstId + ", secondId - " + secondId);
        try {
            Account account = entityManager.find(getClassType(), firstId);
            Account friend = entityManager.find(getClassType(), secondId);
            if (account == null || friend == null) {
                logger.info("End of isFriend method, false");
                return false;
            }
            for (Friend accountFriend : account.getFriends()) {
                if (accountFriend.getSecond_account().equals(friend)) {
                    logger.info("End of isFriend method, true");
                    return true;
                }
            }
            for (Friend friendFriends : friend.getFriends()) {
                if (friendFriends.getSecond_account().equals(account)) {
                    logger.info("End of isFriend method, true");
                    return true;
                }
            }
            logger.info("End of isFriend method, false");
            return false;
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }

    public List<Group> getGroups(int id) throws DaoException {
        logger.info("In getGroups method, id - " + id);
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Participant> criteriaQuery = criteriaBuilder.createQuery(Participant.class);
            Root<Participant> root = criteriaQuery.from(Participant.class);
            CriteriaQuery<Participant> inActive = criteriaQuery.select(root).where(criteriaBuilder.and(
                    criteriaBuilder.isTrue(root.<Boolean>get("is_active")),
                    criteriaBuilder.equal(root.get("account"), id))
            );
            List<Participant> members = entityManager.createQuery(inActive).getResultList();
            List<Group> activeGroups = new ArrayList<>();
            for (Participant participant : members) {
                activeGroups.add(participant.getGroup());
            }
            logger.info("End of getGroups method");
            return activeGroups;
        } catch (RuntimeException e) {
            throw new DaoException(e);
        }
    }
}
