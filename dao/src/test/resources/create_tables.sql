CREATE TABLE account (
  id            INT(11)      NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name          VARCHAR(255) NOT NULL,
  sex           VARCHAR(6)            DEFAULT NULL,
  work_address  VARCHAR(255)          DEFAULT NULL,
  home_address  VARCHAR(255)          DEFAULT NULL,
  email         VARCHAR(255) NOT NULL UNIQUE,
  date_of_birth DATE                  DEFAULT NULL,
  skype         VARCHAR(255)          DEFAULT NULL,
  ICQ           INT(11)               DEFAULT NULL,
  photo         VARCHAR(255)          DEFAULT NULL,
  password      VARCHAR(255)          DEFAULT NULL
);

CREATE TABLE friends (
  first_account_id  INT(11) DEFAULT NULL,
  second_account_id INT(11) DEFAULT NULL,
  is_active         BOOLEAN DEFAULT NULL,
  FOREIGN KEY (second_account_id) REFERENCES account (id)
    ON DELETE CASCADE,
  FOREIGN KEY (first_account_id) REFERENCES account (id)
    ON DELETE CASCADE
);

CREATE TABLE home_numbers (
  id     INT(11)     DEFAULT NULL,
  number VARCHAR(12) DEFAULT NULL,
  FOREIGN KEY (id) REFERENCES account (id)
    ON DELETE CASCADE
);

CREATE TABLE work_numbers (
  id     INT(11)     DEFAULT NULL,
  number VARCHAR(12) DEFAULT NULL,
  FOREIGN KEY (id) REFERENCES account (id)
    ON DELETE CASCADE
);

CREATE TABLE groups (
  id         INT(11)      NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name       VARCHAR(255) NOT NULL UNIQUE,
  creator_id INT(11)               DEFAULT NULL,
  photo      VARCHAR(255)          DEFAULT NULL,
  FOREIGN KEY (creator_id) REFERENCES account (id)
    ON DELETE SET NULL
);

CREATE TABLE accounts_in_groups (
  group_id   INT(11) DEFAULT NULL,
  account_id INT(11) DEFAULT NULL,
  is_active  BOOLEAN DEFAULT NULL,
  FOREIGN KEY (account_id) REFERENCES account (id)
    ON DELETE CASCADE,
  FOREIGN KEY (group_id) REFERENCES groups (id)
    ON DELETE CASCADE
);