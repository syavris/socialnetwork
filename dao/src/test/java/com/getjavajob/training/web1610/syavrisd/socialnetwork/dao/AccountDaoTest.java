package com.getjavajob.training.web1610.syavrisd.socialnetwork.dao;

import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Account;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Group;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by Даниил on 08.12.2016.
 */
public class AccountDaoTest extends AbstractTest {

    @Autowired
    private AccountDao accountDao;
    @Autowired
    private GroupDao groupDao;
    private Account account;

    @Before
    public void init() throws ParseException {
        this.account = new Account("Daniil", "MALE", "lewak@mail.ru");
        account.setDateOfBirth("1991-01-23");
        Set<String> home = new HashSet<>();
        home.add("23214");
        home.add("23121");
        account.setHomeNumbers(home);
        Set<String> work = new HashSet<>();
        work.add("25724");
        work.add("2312211");
        account.setWorkNumbers(work);
        account.setHomeAddress("SPb");
        account.setWorkAddress("Moscow");
        account.setICQ(23131);
        account.setSkype("hello");
    }

    @Test
    @Transactional
    public void createTrue() throws DaoException {
        assertEquals(true, accountDao.create(account));
    }

    @Test
    @Transactional
    public void createFalse() throws DaoException {
        accountDao.create(account);
        assertEquals(false, accountDao.create(account));
    }

    @Test
    @Transactional
    public void getNotNull() throws DaoException {
        accountDao.create(account);
        assertEquals(account, accountDao.get(account.getId()));
    }

    @Test
    @Transactional
    public void getNull() throws DaoException {
        assertEquals(null, accountDao.get(account.getId()));
    }

    @Test
    @Transactional
    public void deleteTrue() throws DaoException {
        accountDao.create(account);
        assertEquals(true, accountDao.delete(account.getId()));
    }

    @Test
    @Transactional
    public void deleteFalse() throws DaoException {
        assertEquals(false, accountDao.delete(account.getId()));
    }

    @Test
    @Transactional
    public void deleteAndTryToGet() throws DaoException {
        accountDao.create(account);
        accountDao.delete(account.getId());
        assertEquals(null, accountDao.get(account.getId()));
    }

    @Test
    @Transactional
    public void updateTrue() throws DaoException {
        accountDao.create(account);
        account.setName("Jhon");
        assertEquals(true, accountDao.update(account));
    }

    @Test
    @Transactional
    public void updateFalse() throws DaoException {
        assertEquals(false, accountDao.update(account));
    }

    @Test
    @Transactional
    public void updateAndGet() throws DaoException {
        accountDao.create(account);
        account.setName("Jhon");
        account.getHomeNumbers().add("+79213278493");
        account.getHomeNumbers().remove("23121");
        accountDao.update(account);
        assertEquals(account, accountDao.get(account.getId()));
    }

    @Test
    @Transactional
    public void getByEmail() throws DaoException {
        accountDao.create(account);
        assertEquals(account, accountDao.getByEmail(account.getEmail()));
    }

    @Test
    @Transactional
    public void getAllExists() throws DaoException {
        Account newAccount = new Account("Katya", "FEMALE", "trunspli@mail.ru");
        accountDao.create(account);
        accountDao.create(newAccount);
        List<Account> list = new ArrayList<>();
        list.add(account);
        list.add(newAccount);
        assertEquals(list, accountDao.getAll());
    }

    @Test
    @Transactional
    public void getAllNull() throws DaoException {
        assertEquals(new ArrayList<Account>(), accountDao.getAll());
    }

    @Test
    @Transactional
    public void addToFriendTrue() throws DaoException {
        Account newAccount = new Account("Katya", "FEMALE", "trunspli@mail.ru");
        accountDao.create(account);
        accountDao.create(newAccount);
        assertEquals(true, accountDao.addAccountToGroupOrFriend(account.getId(), newAccount.getId()));
    }

    @Test
    @Transactional
    public void getInActiveFriend() throws DaoException {
        Account newAccount = new Account("Katya", "FEMALE", "trunspli@mail.ru");
        accountDao.create(account);
        accountDao.create(newAccount);
        accountDao.addAccountToGroupOrFriend(newAccount.getId(), account.getId());
        List<Account> danFriends = new ArrayList<>();
        danFriends.add(newAccount);
        assertEquals(danFriends, accountDao.getInActiveFriendsOrMembers(account.getId()));
    }

    @Test
    @Transactional
    public void addToFriendAcceptAndGet() throws DaoException {
        Account newAccount = new Account("Katya", "FEMALE", "trunspli@mail.ru");
        accountDao.create(account);
        accountDao.create(newAccount);
        accountDao.addAccountToGroupOrFriend(account.getId(), newAccount.getId());
        accountDao.acceptFriendOrMember(account.getId(), newAccount.getId());
        List<Account> danFriends = new ArrayList<>();
        danFriends.add(newAccount);
        List<Account> kateFriends = new ArrayList<>();
        kateFriends.add(account);
        assertEquals(danFriends, accountDao.getActiveFriendsOrMembers(account.getId()));
        assertEquals(kateFriends, accountDao.getActiveFriendsOrMembers(newAccount.getId()));
    }

    @Test
    @Transactional
    public void deleteFromFriendsTrue() throws DaoException {
        Account newAccount = new Account("Katya", "FEMALE", "trunspli@mail.ru");
        accountDao.create(account);
        accountDao.create(newAccount);
        accountDao.addAccountToGroupOrFriend(account.getId(), newAccount.getId());
        assertEquals(true, accountDao.deleteFromGroupOrFriend(account.getId(), newAccount.getId()));
    }

    @Test
    @Transactional
    public void deleteFromFriendsFalse() throws DaoException {
        Account newAccount = new Account("Katya", "FEMALE", "trunspli@mail.ru");
        accountDao.create(account);
        accountDao.create(newAccount);
        assertEquals(false, accountDao.deleteFromGroupOrFriend(account.getId(), newAccount.getId()));
    }

    @Test
    @Transactional
    public void isFriendTrue() throws DaoException {
        Account newAccount = new Account("Katya", "FEMALE", "trunspli@mail.ru");
        accountDao.create(account);
        accountDao.create(newAccount);
        accountDao.addAccountToGroupOrFriend(account.getId(), newAccount.getId());
        assertEquals(true, accountDao.isFriend(account.getId(), newAccount.getId()));
        assertEquals(true, accountDao.isFriend(newAccount.getId(), account.getId()));
    }

    @Test
    @Transactional
    public void isFriendFalse() throws DaoException {
        Account newAccount = new Account("Katya", "FEMALE", "trunspli@mail.ru");
        accountDao.create(account);
        accountDao.create(newAccount);
        assertEquals(false, accountDao.isFriend(account.getId(), newAccount.getId()));
        assertEquals(false, accountDao.isFriend(newAccount.getId(), account.getId()));
    }

    @Test
    @Transactional
    public void getAllNamesTest() throws DaoException {
        Account newAccount = new Account("Katya", "FEMALE", "trunspli@mail.ru");
        accountDao.create(account);
        accountDao.create(newAccount);
        accountDao.create(new Account("Petya", "MALE", "jhon"));
        Group group = new Group("Dan", account);
        groupDao.create(group);
        List<String> result = new ArrayList<>();
        result.add(account.getName());
        result.add(group.getName());
        assertEquals(result, accountDao.getAllNames("dan"));
    }

    @Test
    @Transactional
    public void getByName() throws DaoException {
        Account newAccount = new Account("Katya", "FEMALE", "trunspli@mail.ru");
        accountDao.create(account);
        accountDao.create(newAccount);
        assertEquals(2, accountDao.getIdByName("Katya"));
    }

    @Test
    @Transactional
    public void getCountByName() throws DaoException {
        Account newAccount = new Account("Katya", "FEMALE", "trunspli@mail.ru");
        accountDao.create(account);
        accountDao.create(newAccount);
        long count = 2;
        assertEquals(count, (long) accountDao.getCountByNamePart(""));
    }

    @Test
    @Transactional
    public void getGroups() throws DaoException {
        Group group = new Group("Java", account);
        Group secondGroup = new Group("C++", account);
        Account secondAccount = new Account("Mimi", "FEMALE", "gsgsgd");
        Group badGroup = new Group("CSS", secondAccount);
        accountDao.create(account);
        accountDao.create(secondAccount);
        groupDao.create(group);
        groupDao.create(secondGroup);
        groupDao.create(badGroup);
        ArrayList<Group> arrayList = new ArrayList<>();
        arrayList.add(group);
        arrayList.add(secondGroup);
        assertEquals(arrayList, accountDao.getGroups(account.getId()));
    }
}
