package com.getjavajob.training.web1610.syavrisd.socialnetwork.dao;

import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Account;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Group;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Даниил on 10.12.2016.
 */
public class GroupDaoTest extends AbstractTest {

    @Autowired
    private AccountDao accountDao;
    private Account account;
    @Autowired
    private GroupDao groupDao;
    private Group group;

    @Before
    public void init() throws DaoException {
        account = new Account("Daniil", "MALE", "lewak@mail.ru");
        account.setHomeNumbers(new HashSet<String>());
        account.setWorkNumbers(new HashSet<String>());
        accountDao.create(account);
        group = new Group("Java", account);
    }

    @Test
    @Transactional
    public void createTrue() throws DaoException {
        assertEquals(true, groupDao.create(group));
    }

    @Test
    @Transactional
    public void createFalse() throws DaoException {
        groupDao.create(group);
        assertEquals(false, groupDao.create(group));
    }


    @Test
    @Transactional
    public void getNotNull() throws DaoException {
        groupDao.create(group);
        assertEquals(group, groupDao.get(group.getId()));
    }

    @Test
    @Transactional
    public void getNull() throws DaoException {
        assertEquals(null, groupDao.get(group.getId()));
    }

    @Test
    @Transactional
    public void updateTrue() throws DaoException {
        groupDao.create(group);
        group.setName("C++");
        assertEquals(true, groupDao.update(group));
    }

    @Test
    @Transactional
    public void updateFalse() throws DaoException {
        assertEquals(false, groupDao.update(group));
    }

    @Test
    @Transactional
    public void updateAndGet() throws DaoException {
        groupDao.create(group);
        group.setName("C++");
        groupDao.update(group);
        assertEquals(group, groupDao.get(group.getId()));
    }

    @Test
    @Transactional
    public void deleteTrue() throws DaoException {
        groupDao.create(group);
        assertEquals(true, groupDao.delete(group.getId()));
    }

    @Test
    @Transactional
    public void deleteFalse() throws DaoException {
        assertEquals(false, groupDao.delete(group.getId()));
    }

    @Test
    @Transactional
    public void deleteAndTryToGet() throws DaoException {
        groupDao.create(group);
        groupDao.delete(group.getId());
        assertEquals(null, groupDao.get(group.getId()));
    }

    @Test
    @Transactional
    public void getAllExists() throws DaoException {
        Account newAccount = new Account("Katya", "FEMALE", "trunspli@mail.ru");
        accountDao.create(newAccount);
        Group newGroup = new Group("C++", newAccount);
        List<Group> list = new ArrayList<>();
        list.add(group);
        list.add(newGroup);
        groupDao.create(group);
        groupDao.create(newGroup);
        assertEquals(list, groupDao.getAll());
    }

    @Test
    @Transactional
    public void getAllNull() throws DaoException {
        assertEquals(new ArrayList<Group>(), groupDao.getAll());
    }

    @Test
    @Transactional
    public void addToGroupTrue() throws DaoException {
        Account newAccount = new Account("Katya", "FEMALE", "trunspli@mail.ru");
        accountDao.create(newAccount);
        groupDao.create(group);
        assertEquals(true, groupDao.addAccountToGroupOrFriend(group.getId(), newAccount.getId()));
    }

    @Test
    @Transactional
    public void getInActiveMember() throws DaoException {
        Account newAccount = new Account("Katya", "FEMALE", "trunspli@mail.ru");
        accountDao.create(newAccount);
        groupDao.create(group);
        groupDao.addAccountToGroupOrFriend(group.getId(), newAccount.getId());
        List<Account> list = new ArrayList<>();
        list.add(newAccount);
        assertEquals(list, groupDao.getInActiveFriendsOrMembers(group.getId()));
    }

    @Test
    @Transactional
    public void addToGroupAcceptAndGet() throws DaoException {
        Account newAccount = new Account("Katya", "FEMALE", "trunspli@mail.ru");
        accountDao.create(newAccount);
        groupDao.create(group);
        groupDao.addAccountToGroupOrFriend(group.getId(), newAccount.getId());
        groupDao.acceptFriendOrMember(group.getId(), newAccount.getId());
        List<Account> list = new ArrayList<>();
        list.add(account);
        list.add(newAccount);
        assertEquals(list, groupDao.getActiveFriendsOrMembers(group.getId()));
    }

    @Test
    @Transactional
    public void deleteFromGroupTrue() throws DaoException {
        Account newAccount = new Account("Katya", "FEMALE", "trunspli@mail.ru");
        accountDao.create(newAccount);
        groupDao.create(group);
        groupDao.addAccountToGroupOrFriend(group.getId(), newAccount.getId());
        assertEquals(true, groupDao.deleteFromGroupOrFriend(group.getId(), newAccount.getId()));
    }

    @Test
    @Transactional
    public void deleteFromGroupFalse() throws DaoException {
        Account newAccount = new Account("Katya", "FEMALE", "trunspli@mail.ru");
        groupDao.create(group);
        assertEquals(false, groupDao.deleteFromGroupOrFriend(group.getId(), newAccount.getId()));
    }

    @Test
    @Transactional
    public void getByName() throws DaoException {
        groupDao.create(group);
        assertEquals(1, groupDao.getIdByName("Java"));
    }

    @Test
    @Transactional
    public void getCountByName() throws DaoException {
        groupDao.create(group);
        long count = 1;
        assertEquals(count, (long) groupDao.getCountByNamePart("a"));
    }

    @Test
    @Transactional
    public void getAllNames() throws DaoException {
        groupDao.create(group);
        accountDao.create(new Account("Jhon", "MALE", "rghgrjge"));
        accountDao.create(new Account("Frank", "MALE", "fewfwe"));
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Jhon");
        arrayList.add("Java");
        assertEquals(arrayList, groupDao.getAllNames("j"));
    }

    @Test
    @Transactional
    public void isInGroupTrue() throws DaoException {
        Account newAccount = new Account("Katya", "FEMALE", "trunspli@mail.ru");
        accountDao.create(newAccount);
        groupDao.create(group);
        groupDao.addAccountToGroupOrFriend(group.getId(), newAccount.getId());
        assertEquals(true, groupDao.isAccountInGroup(group.getId(), account.getId()));
    }

    @Test
    @Transactional
    public void isInGroupFalse() throws DaoException {
        Account newAccount = new Account("Katya", "FEMALE", "trunspli@mail.ru");
        accountDao.create(newAccount);
        groupDao.create(group);
        assertEquals(true, groupDao.isAccountInGroup(group.getId(), account.getId()));
    }
}
