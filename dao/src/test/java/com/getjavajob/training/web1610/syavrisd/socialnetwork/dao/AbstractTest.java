package com.getjavajob.training.web1610.syavrisd.socialnetwork.dao;

import org.h2.tools.RunScript;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.SQLException;

/**
 * Created by Даниил on 09.12.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:dao-context-override.xml"})
public abstract class AbstractTest {

    @Autowired
    private DataSource dataSource;

    @Before
    public void createTables() throws SQLException, FileNotFoundException {
        RunScript.execute(dataSource.getConnection(), new FileReader(getPath() + "drop_tables.sql"));
        RunScript.execute(dataSource.getConnection(), new FileReader(getPath() + "create_tables.sql"));
    }

    private String getPath() {
        return "D:\\dev\\projects\\getjavajob\\SocialNetwork\\dao\\src\\test\\resources\\";
    }
}
