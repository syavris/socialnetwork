package com.getjavajob.training.web1610.syavrisd.socialnetwork.common;

import javax.persistence.*;
import javax.persistence.Entity;
import java.io.Serializable;

/**
 * Created by Даниил on 19.02.2017.
 */
@Entity
@Table(name = "accounts_in_groups")
public class Participant {

    @EmbeddedId
    private Key key = new Key();

    @OneToOne
    @MapsId("group_id")
    private Group group;

    @ManyToOne
    @MapsId("account_id")
    private Account account;

    private boolean is_active;

    public Participant() {

    }

    public Participant(Group group, Account account, boolean is_active) {
        this.group = group;
        this.account = account;
        this.is_active = is_active;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public boolean isIs_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    @Embeddable
    private static class Key implements Serializable {
        private int group_id;
        private int account_id;
    }
}
