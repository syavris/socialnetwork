package com.getjavajob.training.web1610.syavrisd.socialnetwork.common;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Даниил on 04.12.2016.
 */
@javax.persistence.Entity
@Table(name = "groups")
public class Group extends Entity {

    private String name;

    @OneToOne
    @JoinColumn(name = "creator_id")
    private Account creatorAccount;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "group", orphanRemoval = true)
    @JsonIgnore
    private List<Participant> members;

    private String photo;

    public Group() {
        members = new ArrayList<>();
    }

    public Group(String name, Account creatorAccount) {
        this.name = name;
        this.creatorAccount = creatorAccount;
        members = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Account getCreatorAccount() {
        return creatorAccount;
    }

    public void setCreatorAccount(Account creatorAccount) {
        this.creatorAccount = creatorAccount;
    }

    public List<Participant> getMembers() {
        return members;
    }

    public void setMembers(List<Participant> members) {
        this.members = members;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + getId() +
                ", name='" + getName() + '\'' +
                ", creatorAccount='" + creatorAccount + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        if (!creatorAccount.equals(group.getCreatorAccount())) return false;

        return name.equals(group.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
