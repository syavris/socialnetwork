package com.getjavajob.training.web1610.syavrisd.socialnetwork.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Даниил on 04.12.2016.
 */
@javax.persistence.Entity
@Table(name = "account")
@XStreamAlias("account")
public class Account extends Entity {

    @XStreamAlias("name")
    private String name;
    @XStreamOmitField
    private String password;

    @Enumerated(EnumType.STRING)
    @XStreamAlias("sex")
    private Sex sex;

    @Column(name = "date_of_birth")
    @XStreamAlias("birthday")
    private Date dateOfBirth;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "home_numbers", joinColumns = @JoinColumn(name = "id"))
    @Column(name = "number")
    @XStreamImplicit(itemFieldName = "homeNumber")
    private Set<String> homeNumbers;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "work_numbers", joinColumns = @JoinColumn(name = "id"))
    @Column(name = "number")
    @XStreamImplicit(itemFieldName = "workNumber")
    private Set<String> workNumbers;

    @Column(name = "home_address")
    @XStreamAlias("homeAddress")
    private String homeAddress;

    @Column(name = "work_address")
    @XStreamAlias("workAddress")
    private String workAddress;

    private String email;
    private Integer ICQ;
    private String skype;

    @OneToMany(mappedBy = "first_account", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    @XStreamOmitField
    private List<Friend> friends;

    @XStreamOmitField
    private String photo;

    public Account() {
        homeNumbers = new HashSet<>();
        workNumbers = new HashSet<>();
        friends = new ArrayList<>();
    }

    public Account(String name, String sex, String email) {
        this.name = name;
        setSex(sex);
        this.email = email;
        homeNumbers = new HashSet<>();
        workNumbers = new HashSet<>();
        friends = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = Sex.valueOf(sex);
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        try {
            this.dateOfBirth = new SimpleDateFormat("yyyy-MM-dd" + " HH:mm:ss.SSS").parse(dateOfBirth + " 00:00:00.000");
        } catch (ParseException e) {
            System.out.println("Wrong date format");
        }
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Set<String> getHomeNumbers() {
        return homeNumbers;
    }

    public void setHomeNumbers(Set<String> homeNumbers) {
        this.homeNumbers = homeNumbers;
    }

    public Set<String> getWorkNumbers() {
        return workNumbers;
    }

    public void setWorkNumbers(Set<String> workNumbers) {
        this.workNumbers = workNumbers;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getICQ() {
        return ICQ;
    }

    public void setICQ(Integer ICQ) {
        this.ICQ = ICQ;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public List<Friend> getFriends() {
        return friends;
    }

    public void setFriends(List<Friend> friends) {
        this.friends = friends;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                ", sex=" + sex.name() +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", homeNumbers=" + homeNumbers +
                ", workNumbers=" + workNumbers +
                ", homeAddress=" + homeAddress +
                ", workAddress=" + workAddress +
                ", email='" + email + '\'' +
                ", ICQ=" + ICQ +
                ", skype='" + skype + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (!Objects.equals(ICQ, account.ICQ)) return false;
        if (!name.equals(account.name)) return false;
        if (sex != account.sex) return false;
        if (dateOfBirth != null ? !dateOfBirth.equals(account.dateOfBirth) : account.dateOfBirth != null) return false;
        if (homeNumbers != null ? !homeNumbers.equals(account.homeNumbers) : account.homeNumbers != null) return false;
        if (workNumbers != null ? !workNumbers.equals(account.workNumbers) : account.workNumbers != null) return false;
        if (homeAddress != null ? !homeAddress.equals(account.homeAddress) : account.homeAddress != null) return false;
        if (workAddress != null ? !workAddress.equals(account.workAddress) : account.workAddress != null) return false;
        if (!email.equals(account.email)) return false;
        return skype != null ? skype.equals(account.skype) : account.skype == null;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + sex.hashCode();
        result = 31 * result + (dateOfBirth != null ? dateOfBirth.hashCode() : 0);
        result = 31 * result + (homeNumbers != null ? homeNumbers.hashCode() : 0);
        result = 31 * result + (workNumbers != null ? workNumbers.hashCode() : 0);
        result = 31 * result + (homeAddress != null ? homeAddress.hashCode() : 0);
        result = 31 * result + (workAddress != null ? workAddress.hashCode() : 0);
        result = 31 * result + email.hashCode();
        result = 31 * result + ICQ;
        result = 31 * result + (skype != null ? skype.hashCode() : 0);
        return result;
    }
}
