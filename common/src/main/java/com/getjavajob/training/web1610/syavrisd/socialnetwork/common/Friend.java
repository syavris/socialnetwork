package com.getjavajob.training.web1610.syavrisd.socialnetwork.common;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Даниил on 17.02.2017.
 */
@javax.persistence.Entity
@Table(name = "friends")
public class Friend {

    @EmbeddedId
    private Key key = new Key();

    @ManyToOne
    @MapsId("first_account_id")
    private Account first_account;

    @ManyToOne
    @MapsId("second_account_id")
    private Account second_account;

    private boolean is_active;

    public Friend() {

    }

    public Friend(Account first_account, Account second_account, boolean is_active) {
        this.first_account = first_account;
        this.second_account = second_account;
        this.is_active = is_active;
    }

    public Account getFirst_account() {
        return first_account;
    }

    public void setFirst_account(Account first_account) {
        this.first_account = first_account;
    }

    public Account getSecond_account() {
        return second_account;
    }

    public void setSecond_account(Account second_account) {
        this.second_account = second_account;
    }

    public boolean isIs_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    @Embeddable
    private static class Key implements Serializable {
        private int first_account_id;
        private int second_account_id;
    }


}
