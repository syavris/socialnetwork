package com.getjavajob.training.web1610.syavrisd.socialnetwork.common;

/**
 * Created by Даниил on 04.12.2016.
 */
public enum Sex {

    MALE(), FEMALE()
}
