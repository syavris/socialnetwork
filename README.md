# Yet Another Social Network  
  
** Functionality: **  
  
+ registration  
+ authentication  
+ ajax search with pagination  
+ display profile  
+ edit profile  
+ edit profile from xml  
+ upload avatar  
+ users export to xml  
+ add/confirm/remove friend  
+ create/edit group  
+ add/confirm/remove group member  
  
** Tools: **  
JDK 7, Spring 4, JPA 2 / Hibernate 5, XStream, jQuery 2, AWS SDK, Twitter Bootstrap 3, JUnit 4, Mockito, log4j2, Maven 3, Git / Bitbucket, Tomcat 8, MySQL, IntelliJIDEA 14.  
  
**Notes:**  
MySQL ddl is located in the `db.sql`  
  
The project hosted at https://dssocialnetwork.herokuapp.com  
  
Test account: ivan@mail.ru  
Password: 1234  
  
**Screenshots**  
  
![https://gyazo.com/ddc8b56fbee54810f181fddb5cb406d9](https://i.gyazo.com/ddc8b56fbee54810f181fddb5cb406d9.png)  
![https://gyazo.com/50124eaf9bc48151a3a08d985015bbc6](https://i.gyazo.com/50124eaf9bc48151a3a08d985015bbc6.png)  
![https://gyazo.com/a243ec0e15d640f9071aeac40f4816e7](https://i.gyazo.com/a243ec0e15d640f9071aeac40f4816e7.png)  
![https://gyazo.com/508aa059b54f1ed8e964eb676d27b5d4](https://i.gyazo.com/508aa059b54f1ed8e964eb676d27b5d4.png)  
![https://gyazo.com/1df97d118a70cd89205a17318ea2988d](https://i.gyazo.com/1df97d118a70cd89205a17318ea2988d.png)  
  
--  
**Daniil Syavris**  
Training getJavaJob,   
[http://www.getjavajob.com](http://www.getjavajob.com)