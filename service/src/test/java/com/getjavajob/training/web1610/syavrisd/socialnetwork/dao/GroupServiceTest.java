package com.getjavajob.training.web1610.syavrisd.socialnetwork.dao;

import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Account;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Group;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.service.GroupService;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.service.GroupServiceException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by Даниил on 11.01.2017.
 */
public class GroupServiceTest {

    private static GroupService groupService;
    private static GroupDao groupDao;
    private static Group owner;
    private static Account member;
    private static String filter = "filter";

    @BeforeClass
    public static void init() {
        owner = mock(Group.class);
        member = mock(Account.class);
    }

    @Before
    public void generate() throws InterruptedException, SQLException {
        groupDao = mock(GroupDao.class);
        groupService = new GroupService(groupDao);
    }

    @Test
    public void getGroupById() throws DaoException, GroupServiceException, InterruptedException, SQLException {
        when(groupDao.get(owner.getId())).thenReturn(null);
        assertNull(groupService.getGroupById(owner.getId()));
    }

    @Test
    public void createGroup() throws DaoException, GroupServiceException, InterruptedException, SQLException {
        when(groupDao.create(owner)).thenReturn(true);
        assertEquals(true, groupService.createGroup(owner));
    }

    @Test
    public void updateGroup() throws DaoException, GroupServiceException, InterruptedException, SQLException {
        when(groupDao.update(owner)).thenReturn(true);
        assertEquals(true, groupService.updateGroup(owner));
    }

    @Test
    public void deleteGroup() throws DaoException, GroupServiceException, InterruptedException, SQLException {
        when(groupDao.delete(owner.getId())).thenReturn(true);
        assertEquals(true, groupService.deleteGroup(owner));
    }

    @Test
    public void addAccountToGroup() throws DaoException, GroupServiceException, InterruptedException, SQLException {
        when(groupDao.addAccountToGroupOrFriend(owner.getId(), member.getId())).thenReturn(true);
        assertEquals(true, groupService.addAccountToGroup(owner, member));
    }

    @Test
    public void acceptMember() throws DaoException, GroupServiceException, InterruptedException, SQLException {
        when(groupDao.acceptFriendOrMember(owner.getId(), member.getId())).thenReturn(true);
        assertEquals(true, groupService.acceptMember(owner, member));
    }

    @Test
    public void deleteMember() throws DaoException, GroupServiceException, InterruptedException, SQLException {
        when(groupDao.deleteFromGroupOrFriend(owner.getId(), member.getId())).thenReturn(true);
        assertEquals(true, groupService.deleteMember(owner, member));
    }

    @Test
    public void getActiveMembers() throws DaoException, GroupServiceException, InterruptedException, SQLException {
        when(groupDao.getActiveFriendsOrMembers(owner.getId())).thenReturn(new ArrayList<Account>());
        assertEquals(new ArrayList<Account>(), groupService.getActiveMembers(owner));
        verify(groupDao).getActiveFriendsOrMembers(owner.getId());
    }

    @Test
    public void getInActiveMembers() throws DaoException, GroupServiceException, InterruptedException, SQLException {
        when(groupDao.getInActiveFriendsOrMembers(owner.getId())).thenReturn(new ArrayList<Account>());
        assertEquals(new ArrayList<Account>(), groupService.getInActiveMembers(owner));
    }

    @Test
    public void getAllGroups() throws DaoException, GroupServiceException, InterruptedException, SQLException {
        when(groupDao.getAll()).thenReturn(new ArrayList<Group>());
        assertEquals(new ArrayList<Group>(), groupService.getAllGroups());
    }

    @Test
    public void getNames() throws DaoException, GroupServiceException {
        when(groupDao.getAllNames(filter)).thenReturn(new ArrayList<String>());
        assertEquals(new ArrayList<String>(), groupService.getNames(filter));
    }

    @Test
    public void isFriend() throws DaoException, GroupServiceException, InterruptedException, SQLException {
        when(groupDao.isAccountInGroup(owner.getId(), member.getId())).thenReturn(true);
        assertEquals(true, groupService.isAccountInGroup(owner, member));
    }
}
