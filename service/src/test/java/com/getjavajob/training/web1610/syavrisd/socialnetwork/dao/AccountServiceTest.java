package com.getjavajob.training.web1610.syavrisd.socialnetwork.dao;

import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Account;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Group;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.service.AccountService;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.service.AccountServiceException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

/**
 * Created by Даниил on 15.12.2016.
 */
public class AccountServiceTest {

    private static AccountService accountService;
    private static AccountDao accountDao;
    private static Account owner;
    private static Account friend;
    private static String filter = "filter";

    @BeforeClass
    public static void init() {
        owner = mock(Account.class);
        friend = mock(Account.class);
    }

    @Before
    public void generate() throws InterruptedException, SQLException {
        accountDao = mock(AccountDao.class);
        accountService = new AccountService(accountDao);
    }

    @Test
    public void getAccountByEmail() throws DaoException, AccountServiceException, InterruptedException, SQLException {
        when(accountDao.getByEmail(owner.getEmail())).thenReturn(null);
        assertNull(accountService.getAccountByEmail(owner.getEmail()));
    }

    @Test
    public void getAccountById() throws DaoException, AccountServiceException, InterruptedException, SQLException {
        when(accountDao.get(owner.getId())).thenReturn(null);
        assertNull(accountService.getAccountById(owner.getId()));
    }

    @Test
    public void createAccount() throws DaoException, AccountServiceException, InterruptedException, SQLException {
        when(accountDao.create(owner)).thenReturn(true);
        assertEquals(true, accountService.createAccount(owner));
    }

    @Test
    public void updateAccount() throws DaoException, AccountServiceException, InterruptedException, SQLException {
        when(accountDao.update(owner)).thenReturn(true);
        assertEquals(true, accountService.updateAccount(owner));
    }

    @Test
    public void deleteAccount() throws DaoException, AccountServiceException, InterruptedException, SQLException {
        when(accountDao.delete(owner.getId())).thenReturn(true);
        assertEquals(true, accountService.deleteAccount(owner));
    }

    @Test
    public void addFriendAccount() throws DaoException, AccountServiceException, InterruptedException, SQLException {
        when(accountDao.addAccountToGroupOrFriend(owner.getId(), friend.getId())).thenReturn(true);
        assertEquals(true, accountService.addFriendToAccount(owner, friend));
    }

    @Test
    public void acceptFriendAccount() throws DaoException, AccountServiceException, InterruptedException, SQLException {
        when(accountDao.acceptFriendOrMember(owner.getId(), friend.getId())).thenReturn(true);
        assertEquals(true, accountService.acceptFriend(owner, friend));
    }

    @Test
    public void deleteFriendAccount() throws DaoException, AccountServiceException, InterruptedException, SQLException {
        when(accountDao.deleteFromGroupOrFriend(owner.getId(), friend.getId())).thenReturn(true);
        assertEquals(true, accountService.deleteFriend(owner, friend));
    }

    @Test
    public void getActiveFriends() throws DaoException, AccountServiceException, InterruptedException, SQLException {
        when(accountDao.getActiveFriendsOrMembers(owner.getId())).thenReturn(new ArrayList<Account>());
        assertEquals(new ArrayList<Account>(), accountService.getActiveFriends(owner));
        verify(accountDao).getActiveFriendsOrMembers(owner.getId());
    }

    @Test
    public void getInActiveFriends() throws DaoException, AccountServiceException, InterruptedException, SQLException {
        when(accountDao.getInActiveFriendsOrMembers(owner.getId())).thenReturn(new ArrayList<Account>());
        assertEquals(new ArrayList<Account>(), accountService.getInActiveFriends(owner));
    }

    @Test
    public void getAllAccounts() throws DaoException, AccountServiceException, InterruptedException, SQLException {
        when(accountDao.getAll()).thenReturn(new ArrayList<Account>());
        assertEquals(new ArrayList<Account>(), accountService.getAllAccounts());
    }

    @Test
    public void getGroups() throws DaoException, AccountServiceException, InterruptedException, SQLException {
        when(accountDao.getGroups(owner.getId())).thenReturn(new ArrayList<Group>());
        assertEquals(new ArrayList<Group>(), accountService.getGroups(owner));
    }

    @Test
    public void getNames() throws DaoException, AccountServiceException {
        when(accountDao.getAllNames(filter)).thenReturn(new ArrayList<String>());
        assertEquals(new ArrayList<String>(), accountService.getNames(filter));
    }

    @Test
    public void isAccountInGroup() throws DaoException, AccountServiceException, InterruptedException, SQLException {
        when(accountDao.isFriend(owner.getId(), friend.getId())).thenReturn(true);
        assertEquals(true, accountService.isFriend(owner, friend));
    }
}
