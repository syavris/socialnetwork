package com.getjavajob.training.web1610.syavrisd.socialnetwork.service;

/**
 * Created by Даниил on 29.12.2016.
 */
public class GroupServiceException extends Exception {

    public GroupServiceException(Throwable cause) {
        super(cause);
    }
}
