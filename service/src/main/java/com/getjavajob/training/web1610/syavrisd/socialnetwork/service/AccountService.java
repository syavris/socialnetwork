package com.getjavajob.training.web1610.syavrisd.socialnetwork.service;

import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Account;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Group;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.dao.AccountDao;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.dao.DaoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Даниил on 15.12.2016.
 */
@Service
public class AccountService {

    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);
    private AccountDao accountDao;

    @Autowired
    public AccountService(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Transactional
    public Account getAccountByEmail(String email) throws AccountServiceException {
        logger.info("In getAccountByEmail method, email - " + email);
        try {
            return accountDao.getByEmail(email);
        } catch (DaoException e) {
            throw new AccountServiceException(e);
        } finally {
            logger.info("End of getAccountByEmail method");
        }
    }

    @Transactional
    public Account getAccountById(int id) throws AccountServiceException {
        logger.info("In getAccountById method, id - " + id);
        try {
            return accountDao.get(id);
        } catch (DaoException e) {
            throw new AccountServiceException(e);
        } finally {
            logger.info("End of getAccountById method");
        }
    }

    @Transactional
    public boolean createAccount(Account account) throws AccountServiceException {
        logger.info("In createAccount method, account - " + account.toString());
        try {
            return accountDao.create(account);
        } catch (DaoException e) {
            throw new AccountServiceException(e);
        } finally {
            logger.info("End of createAccount method");
        }
    }

    @Transactional
    public boolean updateAccount(Account account) throws AccountServiceException {
        logger.info("In updateAccount method, account - " + account.toString());
        try {
            return accountDao.update(account);
        } catch (DaoException e) {
            throw new AccountServiceException(e);
        } finally {
            logger.info("End of updateAccount method");
        }
    }

    @Transactional
    public boolean deleteAccount(Account account) throws AccountServiceException {
        logger.info("In deleteAccount method, account - " + account.toString());
        try {
            return accountDao.delete(account.getId());
        } catch (DaoException e) {
            throw new AccountServiceException(e);
        } finally {
            logger.info("End of deleteAccount method");
        }
    }

    @Transactional
    public boolean addFriendToAccount(Account owner, Account friend) throws AccountServiceException {
        logger.info("In addFriendToAccount method, owner - " + owner.toString() + ", friend - " + friend.toString());
        try {
            return accountDao.addAccountToGroupOrFriend(owner.getId(), friend.getId());
        } catch (DaoException e) {
            throw new AccountServiceException(e);
        } finally {
            logger.info("End of addFriendToAccount method");
        }
    }

    @Transactional
    public boolean acceptFriend(Account owner, Account friend) throws AccountServiceException {
        logger.info("In acceptFriend method, owner - " + owner.toString() + ", friend - " + friend.toString());
        try {
            return accountDao.acceptFriendOrMember(owner.getId(), friend.getId());
        } catch (DaoException e) {
            throw new AccountServiceException(e);
        } finally {
            logger.info("End of acceptFriend method");
        }
    }

    @Transactional
    public boolean deleteFriend(Account owner, Account friend) throws AccountServiceException {
        logger.info("In deleteFriend method, owner - " + owner.toString() + ", friend - " + friend.toString());
        try {
            return accountDao.deleteFromGroupOrFriend(owner.getId(), friend.getId());
        } catch (DaoException e) {
            throw new AccountServiceException(e);
        } finally {
            logger.info("End of deleteFriend method");
        }
    }

    @Transactional
    public List<Account> getActiveFriends(Account account) throws AccountServiceException {
        logger.info("In getActiveFriends method, account - " + account.toString());
        try {
            return accountDao.getActiveFriendsOrMembers(account.getId());
        } catch (DaoException e) {
            throw new AccountServiceException(e);
        } finally {
            logger.info("End of getActiveFriends method");
        }
    }

    @Transactional
    public List<Account> getInActiveFriends(Account account) throws AccountServiceException {
        logger.info("In getInActiveFriends method, account - " + account.toString());
        try {
            return accountDao.getInActiveFriendsOrMembers(account.getId());
        } catch (DaoException e) {
            throw new AccountServiceException(e);
        } finally {
            logger.info("End of getInActiveFriends method");
        }
    }

    @Transactional
    public List<Account> getAllAccounts() throws AccountServiceException {
        logger.info("In getAllAccounts method");
        try {
            return accountDao.getAll();
        } catch (DaoException e) {
            throw new AccountServiceException(e);
        } finally {
            logger.info("End of getAllAccounts method");
        }
    }

    @Transactional
    public List<Group> getGroups(Account account) throws AccountServiceException {
        logger.info("In getGroups method, account - " + account.toString());
        try {
            return accountDao.getGroups(account.getId());
        } catch (DaoException e) {
            throw new AccountServiceException(e);
        } finally {
            logger.info("End of getGroups method");
        }
    }

    @Transactional
    public List<String> getNames(String filter) throws AccountServiceException {
        logger.info("In getNames method, filter - " + filter);
        try {
            return accountDao.getAllNames(filter);
        } catch (DaoException e) {
            throw new AccountServiceException(e);
        } finally {
            logger.info("End of getNames method");
        }
    }

    @Transactional
    public boolean isFriend(Account firstAccount, Account secondAccount) throws AccountServiceException {
        logger.info("In isFriend method, firstAccount - " + firstAccount.toString() + ", secondAccount - " + secondAccount.toString());
        try {
            return accountDao.isFriend(firstAccount.getId(), secondAccount.getId());
        } catch (DaoException e) {
            throw new AccountServiceException(e);
        } finally {
            logger.info("End of isFriend method");
        }
    }

    @Transactional
    public int getIdByName(String name) throws AccountServiceException {
        logger.info("In getIdByName method, name - " + name);
        try {
            return accountDao.getIdByName(name);
        } catch (DaoException e) {
            throw new AccountServiceException(e);
        } finally {
            logger.info("End of getIdByName method");
        }
    }

    @Transactional
    public List<Account> getByNamePart(String name, int from, int to) throws AccountServiceException {
        logger.info("In getByNamePart method, name - " + name);
        try {
            return accountDao.getByNamePart(name, from, to);
        } catch (DaoException e) {
            throw new AccountServiceException(e);
        } finally {
            logger.info("End of getByNamePart method");
        }
    }

    @Transactional
    public Long getCountByNamePart(String name) throws AccountServiceException {
        logger.info("In getCountByNamePart method, name - " + name);
        try {
            return accountDao.getCountByNamePart(name);
        } catch (DaoException e) {
            throw new AccountServiceException(e);
        } finally {
            logger.info("End of getCountByNamePart method");
        }
    }
}
