package com.getjavajob.training.web1610.syavrisd.socialnetwork.service;

import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Account;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.common.Group;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.dao.DaoException;
import com.getjavajob.training.web1610.syavrisd.socialnetwork.dao.GroupDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Даниил on 29.12.2016.
 */
@Service
public class GroupService {

    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);
    private GroupDao groupDao;

    @Autowired
    public GroupService(GroupDao groupDao) {
        this.groupDao = groupDao;
    }

    @Transactional
    public Group getGroupById(int id) throws GroupServiceException {
        logger.info("In getGroupById method, id - " + id);
        try {
            return groupDao.get(id);
        } catch (DaoException e) {
            throw new GroupServiceException(e);
        } finally {
            logger.info("End of getGroupById method");
        }
    }

    @Transactional
    public boolean createGroup(Group group) throws GroupServiceException {
        logger.info("In createGroup method, group - " + group.toString());
        try {
            return groupDao.create(group);
        } catch (DaoException e) {
            throw new GroupServiceException(e);
        } finally {
            logger.info("End of createGroup method");
        }
    }

    @Transactional
    public boolean updateGroup(Group group) throws GroupServiceException {
        logger.info("In updateGroup method, group - " + group.toString());
        try {
            return groupDao.update(group);
        } catch (DaoException e) {
            throw new GroupServiceException(e);
        } finally {
            logger.info("End of updateGroup method");
        }
    }

    @Transactional
    public boolean deleteGroup(Group group) throws GroupServiceException {
        logger.info("In deleteGroup method, group - " + group.toString());
        try {
            return groupDao.delete(group.getId());
        } catch (DaoException e) {
            throw new GroupServiceException(e);
        } finally {
            logger.info("End of deleteGroup method");
        }
    }

    @Transactional
    public boolean addAccountToGroup(Group owner, Account member) throws GroupServiceException {
        logger.info("In addAccountToGroup method, group + " + owner.toString() + ", account - " + member.toString());
        try {
            return groupDao.addAccountToGroupOrFriend(owner.getId(), member.getId());
        } catch (DaoException e) {
            throw new GroupServiceException(e);
        } finally {
            logger.info("End of addAccountToGroup method");
        }
    }

    @Transactional
    public boolean acceptMember(Group owner, Account member) throws GroupServiceException {
        logger.info("In acceptMember method, group + " + owner.toString() + ", account - " + member.toString());
        try {
            return groupDao.acceptFriendOrMember(owner.getId(), member.getId());
        } catch (DaoException e) {
            throw new GroupServiceException(e);
        } finally {
            logger.info("End of acceptMember method");
        }
    }

    @Transactional
    public boolean deleteMember(Group owner, Account member) throws GroupServiceException {
        logger.info("In deleteMember method, group + " + owner.toString() + ", account - " + member.toString());
        try {
            return groupDao.deleteFromGroupOrFriend(owner.getId(), member.getId());
        } catch (DaoException e) {
            throw new GroupServiceException(e);
        } finally {
            logger.info("End of deleteMember method");
        }
    }

    @Transactional
    public boolean isAccountInGroup(Group owner, Account member) throws GroupServiceException {
        logger.info("In isAccountInGroup method, group + " + owner.toString() + ", account - " + member.toString());
        try {
            return groupDao.isAccountInGroup(owner.getId(), member.getId());
        } catch (DaoException e) {
            throw new GroupServiceException(e);
        } finally {
            logger.info("End of isAccountInGroup method");
        }
    }

    @Transactional
    public List<Account> getActiveMembers(Group group) throws GroupServiceException {
        logger.info("In getActiveMembers method, group - " + group.toString());
        try {
            return groupDao.getActiveFriendsOrMembers(group.getId());
        } catch (DaoException e) {
            throw new GroupServiceException(e);
        } finally {
            logger.info("End of getActiveMembers method");
        }
    }

    @Transactional
    public List<Account> getInActiveMembers(Group group) throws GroupServiceException {
        logger.info("In getInActiveMembers method, group - " + group.toString());
        try {
            return groupDao.getInActiveFriendsOrMembers(group.getId());
        } catch (DaoException e) {
            throw new GroupServiceException(e);
        } finally {
            logger.info("End of getInActiveMembers method");
        }
    }

    @Transactional
    public List<Group> getAllGroups() throws GroupServiceException {
        logger.info("In getAllGroups method");
        try {
            return groupDao.getAll();
        } catch (DaoException e) {
            throw new GroupServiceException(e);
        } finally {
            logger.info("End of getAllGroups method");
        }
    }

    @Transactional
    public List<String> getNames(String filter) throws GroupServiceException {
        logger.info("In getNames method, filter - " + filter);
        try {
            return groupDao.getAllNames(filter);
        } catch (DaoException e) {
            throw new GroupServiceException(e);
        } finally {
            logger.info("End of getNames method");
        }
    }

    @Transactional
    public int getIdByName(String name) throws GroupServiceException {
        logger.info("In getIdByName method, name - " + name);
        try {
            return groupDao.getIdByName(name);
        } catch (DaoException e) {
            throw new GroupServiceException(e);
        } finally {
            logger.info("End of getIdByName method");
        }
    }

    @Transactional
    public List<Group> getByNamePart(String name, int from, int to) throws GroupServiceException {
        logger.info("In getByNamePart method, name - " + name);
        try {
            return groupDao.getByNamePart(name, from, to);
        } catch (DaoException e) {
            throw new GroupServiceException(e);
        } finally {
            logger.info("End of getByNamePart method");
        }
    }

    @Transactional
    public Long getCountByNamePart(String name) throws GroupServiceException {
        logger.info("In getCountByNamePart method, name - " + name);
        try {
            return groupDao.getCountByNamePart(name);
        } catch (DaoException e) {
            throw new GroupServiceException(e);
        } finally {
            logger.info("End of getCountByNamePart method");
        }
    }

    @Transactional
    public Group getGroupByName(String name) throws GroupServiceException {
        logger.info("In getGroupByName method, name - " + name);
        try {
            return groupDao.getByName(name);
        } catch (DaoException e) {
            throw new GroupServiceException(e);
        } finally {
            logger.info("End of getGroupByName method");
        }
    }
}
