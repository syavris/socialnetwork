package com.getjavajob.training.web1610.syavrisd.socialnetwork.service;

/**
 * Created by Даниил on 19.12.2016.
 */
public class AccountServiceException extends Exception {

    public AccountServiceException(Throwable cause) {
        super(cause);
    }
}
